# **Backup Restore app**

## Description
---
This is a network application for creating backup of network devices.  
Application using Telnet or SSH protocol to connect with choosen Cisco company device and then creates backup of his
configuration files or the IOS operation system.

## Technologies
---
* Spring Core (5.1.3.RELEASE)
* Spring Security (5.1.2.RELEASE)  
* Hibernate (5.2.16.Final)  
* H2 Database Engine (1.4.196)
* JavaFX 8

## Requirements
---
For building and running the application you need:

* JDK 1.8
* Maven 3

## Build Tool
---
* Maven (3.6.1)

## How to run
---
Java standalone executable: 

```
mvn package  
java -jar target/BackupRestoreapp.jar 
```

## Screenshots

### Start menu
---
![](images/startmenu.png)

### Main window
---
![](images/main.png)

### Server Tftp window
---
![](images/serverTFTP.png)

### Add network device window
---
![](images/addDevice.png)


