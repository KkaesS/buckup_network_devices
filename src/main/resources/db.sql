CREATE SCHEMA IF NOT EXISTS `backup_of_network_devices`;

CREATE TABLE IF NOT EXISTS `users` (
	`username` varchar(50) NOT NULL,
    `password` varchar(128) not null,
    `enabled` tinyint(1) not null,
	 PRIMARY KEY (`username`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `authorities_idx_1` (`username`,`authority`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `tftp` (
	`id` int(10) not null AUTO_INCREMENT,
	`name` varchar(128),
	`ip` varchar(14) not null,
	`username`  varchar(50) not null,
	 PRIMARY KEY (`id`),
	 FOREIGN KEY (`username`) references `users` (`username`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `devices` (
	`id` int(10) not null auto_increment,
  `name` varchar(128) ,
  `ip` varchar(14) not null,
	`password_vty` varchar(128) ,
	`password` varchar(128) ,
  `username` varchar(50) default null,
	`tftp_server_id` int(10) not null,
	`passwordSSH` varchar(128),
	`loginSSH` varchar(128),
	`connectionType` varchar(128),
    primary key (`id`),
    key `fk_user_idx` (`username`),
    constraint `fk_user` foreign key (`username`) references `users` (`username`),
	  constraint `fk_tftp_server` foreign key (`tftp_server_id`) references `tftp` (`id`)

)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `list_of_devices` (
	`id` int(10) not null auto_increment,
    `name` varchar(128) not null,
    `username` varchar(50) default null,
    primary key (`id`),
    foreign key (`username`) references `users` (`username`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `list_of_devices_has_devices` (
	`list_id` int(10) not null,
    `device_id` int(10) not null,
    primary key (`list_id`,`device_id`),
    foreign key (`list_id`) references `list_of_devices` (`id`),
	foreign key (`device_id`) references `devices` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;