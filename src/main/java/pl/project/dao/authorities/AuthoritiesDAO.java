package pl.project.dao.authorities;

import pl.project.entity.Authorities;

/**
 * @author Konrad
 * date 20/12/2018
 * project_name Backup_Network_Devices
 */
public interface AuthoritiesDAO {
    void saveAuthority(Authorities newAuthorities);
}
