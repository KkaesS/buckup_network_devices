package pl.project.dao.authorities;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.project.entity.Authorities;

/**
 * @author Konrad
 * date 20/12/2018
 * project_name Backup_Network_Devices
 */
@Repository
public class AuthoritiesDAOImp implements AuthoritiesDAO {

    private SessionFactory sessionFactory;

    @Autowired
    AuthoritiesDAOImp(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveAuthority(Authorities newAuthorities) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newAuthorities);
    }
}
