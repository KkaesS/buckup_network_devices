package pl.project.dao.user;

import pl.project.entity.Device;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */
public interface UserDAO {

    List<User> getUsers();

    void saveUser(User newUser);

    User getUser(String username);

    User findByEmail(String email);

    void deleteUser(int id);

}
