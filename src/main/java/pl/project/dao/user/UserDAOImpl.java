package pl.project.dao.user;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.project.entity.Device;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */

@Repository
public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    private Session session;

    @Autowired
    UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getUsers() {
        session = sessionFactory.getCurrentSession();
        return session.createQuery("from User", User.class).getResultList(); // TODO: maybe "from Users"
    }

    @Override
    public void saveUser(User newUser) {
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newUser);
    }

    @Override
    public User getUser(String username) {
        session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :username");
        query.setParameter("username", username);
        return query.uniqueResult();
    }


    @Override
    public User findByEmail(String theEmail) {
        session = sessionFactory.getCurrentSession();

        Query query = session.createQuery("from User where email = :theEmail");
        query.setParameter("theEmail", theEmail);
        return (User) query.uniqueResult();
    }


    @Override
    public void deleteUser(int id) {
        session = sessionFactory.getCurrentSession();
        session.delete(session.get(User.class, id));
    }
}
