package pl.project.dao.listOfDevices;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.project.entity.Device;
import pl.project.entity.ListOfDevices;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 29/12/2018
 * project_name Backup_Network_Devices
 */

@Repository
public class ListOfDevicesDAOImpl implements ListOfDevicesDAO {

    private SessionFactory sessionFactory;

    private Session session;

    @Autowired
    ListOfDevicesDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveList(ListOfDevices newListOfDevices) {
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newListOfDevices);
    }


    @Override
    public ListOfDevices getList(int id) {
        session = sessionFactory.getCurrentSession();
        return session.get(ListOfDevices.class, id);
    }

    @Override
    public ListOfDevices getList(String name) {
        session = sessionFactory.getCurrentSession();
        Query<ListOfDevices> query = session.createQuery("from ListOfDevices where name = :name");
        query.setParameter("name", name);
        return query.setMaxResults(1).uniqueResult();
    }

    @Override
    public List<ListOfDevices> getAllUserList(User user) {
        session = sessionFactory.getCurrentSession();
        Query<ListOfDevices> query = session.createQuery("from ListOfDevices where user = :user");
        query.setParameter("user", user);

        return query.getResultList();
    }

    @Override
    public void deleteList(String name) {
        session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete ListOfDevices where name = :name");
        query.setParameter("name", name);
        query.executeUpdate();
    }

}
