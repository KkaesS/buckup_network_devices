package pl.project.dao.tftp;

import pl.project.entity.Device;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */
public interface TftpDAO {

    void saveTftp(Tftp tftp);

    void deleteTftp(Tftp tftp);

    List<Tftp> getTftpListByUser(User user);

    void deleteAllUserTftp(User user);

    boolean isTftpInUse(Tftp tftp);
}
