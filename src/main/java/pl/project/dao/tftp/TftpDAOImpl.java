package pl.project.dao.tftp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.project.entity.Device;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */
@Repository
public class TftpDAOImpl implements TftpDAO {

    private SessionFactory sessionFactory;
    private Session session;

    @Autowired
    public TftpDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveTftp(Tftp tftp) {
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(tftp);
    }

    @Override
    public void deleteTftp(Tftp tftp) {
        session = sessionFactory.getCurrentSession();
        session.delete(tftp);
    }


    @Override
    public List<Tftp> getTftpListByUser(User user) {
        session = sessionFactory.getCurrentSession();
        Query<Tftp> query = session.createQuery("from Tftp where user = :user");
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Override
    public void deleteAllUserTftp(User user) {
        session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("delete from Tftp where user = :user");
        query.setParameter("user", user);
        query.executeUpdate();
    }

    @Override
    public boolean isTftpInUse(Tftp tftp) {
        session = sessionFactory.getCurrentSession();
        Query<Tftp> query = session.createQuery("from Device where tftp = :tftp");
        query.setParameter("tftp", tftp);

        return query.setMaxResults(1).uniqueResult() != null;
    }
}
