package pl.project.dao.device;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.project.entity.Device;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */
@Repository
public class DeviceDAOImpl implements DeviceDAO {

    private SessionFactory sessionFactory;

    private Session session;

    @Autowired
    DeviceDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Device> getDevices(User user) {
        session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from Device where user = :user");
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Override
    public void saveDevice(Device newDevice) {
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(newDevice);
    }

    @Override
    public Device getDevice(String ip) {
        session = sessionFactory.getCurrentSession();
        Query<Device> query = session.createQuery("from Device where ip = :ip");
        query.setParameter("ip", ip);
        return query.setMaxResults(1).uniqueResult();
    }


    @Override
    public void deleteDevice(Device device) {
        session = sessionFactory.getCurrentSession();
        session.delete(device);
    }

    @Override
    public void deleteAllUserDevices(User user) {
        session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("delete from Device where user = :user");
        query.setParameter("user", user);
        query.executeUpdate();
    }
}
