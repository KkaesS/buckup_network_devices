package pl.project.connection;

import javassist.bytecode.stackmap.TypeData;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 07/01/2019
 * project_name Backup_Network_Devices
 */
public class BackupRestoreDevice {
    private final java.util.logging.Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private IConfiguration configuration;
    private Telnet telnet;
    private SSH ssh;

    public BackupRestoreDevice(IConfiguration configuration) {
        this.configuration = configuration;
    }

    public boolean backup(String ipDevice, String filename, String ipTftp, String passwordVty, String passwordEnable, String loginSSH,
                          String passwordSSH, IConnection connection) throws IOException {

        log.info("backup");
        final String command = configuration.backup();

        return getInstance(ipDevice, filename, ipTftp, passwordVty, passwordEnable, loginSSH, passwordSSH, connection, command);
    }

    public boolean restore(String ipDevice, String filename, String ipTftp, String passwordVty, String
            passwordEnable, String loginSSH, String passwordSSH, IConnection connection) throws IOException {

        log.info("restore");
        final String command = configuration.restore();

        return getInstance(ipDevice, filename, ipTftp, passwordVty, passwordEnable, loginSSH, passwordSSH, connection, command);
    }

    @NotNull
    public Boolean getInstance(String ipDevice, String filename, String ipTftp, String passwordVty, String passwordEnable,
                               String loginSSH, String passwordSSH, IConnection connection, String command) throws IOException {

        if (connection instanceof Telnet) {
            telnet = new Telnet(ipDevice);
            telnet.setFilename(filename);
            telnet.setIpTftp(ipTftp);
            telnet.setPasswordVty(passwordVty);
            telnet.setPassword(passwordEnable);
            telnet.setCommand(command);
            if (loginSSH != null) telnet.setLogin(loginSSH);
            log.info("Telnet object: " + telnet.toString());

            return telnet.connect();

        } else if (connection instanceof SSH) {
            ssh = new SSH();
            ssh.setIpDevice(ipDevice);
            ssh.setFilename(filename);
            ssh.setIpTftp(ipTftp);
            ssh.setPassword(passwordEnable);
            ssh.setCommand(command);
            ssh.setLogin(loginSSH);
            ssh.setPasswordSSH(passwordSSH);
            log.info("SSH object: " + ssh.toString());

            return ssh.connect();
        }
        return false;
    }

    public Telnet getTelnet() {
        return telnet;
    }

    public SSH getSsh() {
        return ssh;
    }
}

