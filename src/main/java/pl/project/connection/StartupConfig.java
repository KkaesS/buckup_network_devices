package pl.project.connection;

/**
 * @author Konrad
 * date 24/01/2019
 * project_name Backup_Network_Devices
 */
public class StartupConfig implements IConfiguration {
    @Override
    public String backup() {
            return "copy running-config startup-config\r\n\r\n copy startup-config tftp: ";
    }

    @Override
    public String restore() {
            return "copy tftp: startup-config";
    }
}
