package pl.project.connection;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import javafx.application.Platform;
import javassist.bytecode.stackmap.TypeData;
import pl.project.utils.AlertWindow;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 18/02/2019
 * project_name Backup_Network_Devices
 */
public class SSH implements IConnection {
    private final java.util.logging.Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private String ipDevice;
    private String filename;
    private String ipTftp;
    private String password;
    private String command;
    private String login;
    private String passwordSSH;
    private Session session;
    private JSch jsch;

    public SSH() {
        jsch = new JSch();
    }

    @Override
    public boolean connect() throws IOException {
        log.info("connect SSH");
        boolean value = false;

        try {
            session = jsch.getSession(login, ipDevice, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(passwordSSH);
            session.connect();

            log.info(String.valueOf(session.isConnected()));

        } catch (JSchException jschX) {
            Platform.runLater(() -> AlertWindow.error("Nie można połączyć się z urządzeniem!", jschX.getMessage()));
            return false;
        }

        ChannelShell channel = null;
        try {
            channel = (ChannelShell) session.openChannel("shell");
            channel.setPty(false);
            channel.setInputStream(System.in);
            channel.connect();

            PrintStream out = new PrintStream(channel.getOutputStream(), true);
            for (String comm : commands()) {
                out.print(comm);
                out.flush();
            }
            value = printOutput(channel);
            if (!value) {
                out.println("!STH_WRONG\r\n");
                out.flush();
                return false;
            }

        } catch (JSchException e) {
            Platform.runLater(() -> AlertWindow.error("Coś poszło nie tak", e.getMessage()));
        }

        assert channel != null;
        channel.disconnect();
        session.disconnect();
        return true;
    }

    private boolean printOutput(ChannelShell channel) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream in = channel.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            Thread.sleep(5000);
            String line;

            while (reader.ready()) {
                line = String.valueOf((char) reader.read());
                sb.append(line);

                if (line.equals("@") || sb.toString().contains("bytes copied ") || sb.toString().contains("OK"))
                    return true;
                else if (sb.toString().contains("?Invalid host address or name")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowy adres ip", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("% Bad passwords") || sb.toString().contains("% Bad secrets")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowe hasło!", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("% Unknown command")) {
                    Platform.runLater(() -> AlertWindow.error("Nieznana komenda lub nazwa urządzenia lub nie można znaleźć adresu urządzenia", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("%Error opening flash")) {
                    Platform.runLater(() -> AlertWindow.error("Nie znaleziono pliku", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("File not a valid executable")) {
                    Platform.runLater(() -> AlertWindow.error("Plik nie jest prawidłowym plikiem wykonywalnym dla tego systemu", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("%Error parsing filename")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowa nazwa pliku", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("No such file or directory")) {
                    Platform.runLater(() -> AlertWindow.error("Nie ma takiego pliku", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("%Error opening tftp")) {
                    Platform.runLater(() -> AlertWindow.error("Błąd otwierania połączenia tftp.", "Błąd!"));
                    return false;
                } else if (sb.toString().contains("STH_WRONG")) {
                    Platform.runLater(() -> AlertWindow.error("Coś poszło nie tak", "Błąd!"));
                    return false;
                }
            }

        } catch (
                IOException e) {
            Platform.runLater(() -> AlertWindow.error("Coś poszło nie tak", e.getMessage()));
        } catch (
                InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private List<String> commands() {
        List<String> comm = new ArrayList<>();
        comm.add("enable\n");
        comm.add(password + "\n");
        comm.add(command + "\n");

        if (command.contains("copy flash:")) {
            comm.add(filename + "\n");
            comm.add(ipTftp + "\n");
        } else {
            comm.add(ipTftp + "\n");
            comm.add(filename + "\n");
        }
        comm.add("\n");
        comm.add("\n");
        comm.add("!@");

        return comm;
    }

    public void disconnect() {
        session.disconnect();
    }


    public void setIpDevice(String ipDevice) {
        this.ipDevice = ipDevice;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setIpTftp(String ipTftp) {
        this.ipTftp = ipTftp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPasswordSSH(String passwordSSH) {
        this.passwordSSH = passwordSSH;
    }

    @Override
    public String toString() {
        return "SSH{" +
                "ipDevice='" + ipDevice + '\'' +
                ", filename='" + filename + '\'' +
                ", ipTftp='" + ipTftp + '\'' +
                ", password='" + password + '\'' +
                ", command='" + command + '\'' +
                ", login='" + login + '\'' +
                ", passwordSSH='" + passwordSSH + '\'' +
                ", session=" + session +
                ", jsch=" + jsch +
                '}';
    }
}
