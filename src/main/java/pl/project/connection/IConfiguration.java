package pl.project.connection;

/**
 * @author Konrad
 * date 19/01/2019
 * project_name Backup_Network_Devices
 */
public interface IConfiguration {
    String backup();
    String restore();
}
