package pl.project.connection;

/**
 * @author Konrad
 * date 19/01/2019
 * project_name Backup_Network_Devices
 */
public class RunningConfig implements IConfiguration {

    @Override
    public String backup() {
            return "copy running-config tftp: ";
    }
    @Override
    public String restore() {
            return "copy tftp: running-config";
    }
}
