package pl.project.connection;

import javafx.application.Platform;
import javassist.bytecode.stackmap.TypeData;
import org.apache.commons.net.telnet.TelnetClient;
import pl.project.utils.AlertWindow;

import java.io.*;
import java.net.InetAddress;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 18/02/2019
 * project_name Backup_Network_Devices
 */
public class Telnet implements IConnection {
    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private TelnetClient telnetClient;
    private String filename;
    private String ipTftp;
    private String passwordVty;
    private String password;
    private String command;
    private String login;

    public Telnet(String ipDevice) throws IOException {
        telnetClient = new TelnetClient();
        InetAddress host = InetAddress.getByName(ipDevice);
        telnetClient.connect(host, 23);
    }

    public Telnet() {
    }

    @Override
    public boolean connect() {
        log.info("connect TELNET");
        return writeCommand(filename);
    }

    private boolean writeCommand(String filename) {


      log.info("write command method");
        try (Writer writer = new OutputStreamWriter(telnetClient.getOutputStream(), "UTF-8");
             BufferedReader reader = new BufferedReader(new InputStreamReader(telnetClient.getInputStream()))) {

            write(filename, command, ipTftp, passwordVty, password, login, writer);

            log.info("BEFORE WRITE_COMMAND (TELNET)");

            String line;
            while ((line = reader.readLine()) != null) {

                if (line.endsWith("@") || line.contains("bytes copied in"))
                    return true;
                else if (line.contains("?Invalid host address or name")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowy adres ip", "Błąd!"));
                    return false;
                } else if (line.contains("% Bad passwords") || line.contains("% Bad secrets")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowe hasło!", "Błąd!"));
                    return false;
                } else if (line.contains("% Login invalid")) {
                    Platform.runLater(() -> AlertWindow.error("Nieprawidłowy login!", "Błąd!"));
                    return false;
                } else if (line.contains("(Timed out)") || line.contains("Error")) {
                    Platform.runLater(() -> AlertWindow.error("Błąd otwierania połączenia tftp.", "Błąd!"));
                    return false;
                }
            }

        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void write(String filename, String command, String tftpIp, String passwordVty, String password, String login, Writer writer) throws IOException {

        log.info("write method");
        loginToDevice(login, passwordVty, password, writer);


        writer.write(command + "\r\n");

        if (command.contains("copy flash:")) {
            writer.write(filename + "\r\n");
            writer.write(tftpIp + "\r\n");
        } else {
            writer.write(tftpIp + "\r\n");
            writer.write(filename + "\r\n");
        }

        writer.write("\n\r");
        writer.write("\n\r");
        writer.write("!@\r\n");
        writer.flush();
    }

    private void loginToDevice(String login, String passwordVty, String password, Writer writer) throws IOException {
        log.info("login device");
        if (login != null)
            writer.write(login + "\r\n");
        writer.write(passwordVty + "\r\n");
        writer.write("enable\r\n");
        writer.write(password + "\r\n");
    }

    public void disconnect() {
        try {
            telnetClient.getOutputStream().write("^]\r\n".getBytes());
            telnetClient.getOutputStream().flush();
        } catch (IOException | NullPointerException e) {
            Platform.runLater(() -> AlertWindow.error("Przerwano połączenie!", "Disconnect"));
        }
    }

    void setFilename(String filename) {
        this.filename = filename;
    }

    void setIpTftp(String ipTftp) {
        this.ipTftp = ipTftp;
    }

    void setPasswordVty(String passwordVty) {
        this.passwordVty = passwordVty;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    void setCommand(String command) {
        this.command = command;
    }

    void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Telnet{" +
                "telnetClient=" + telnetClient +
                ", filename='" + filename + '\'' +
                ", ipTftp='" + ipTftp + '\'' +
                ", passwordVty='" + passwordVty + '\'' +
                ", password='" + password + '\'' +
                ", command='" + command + '\'' +
                ", login='" + login + '\'' +
                '}';
    }
}
