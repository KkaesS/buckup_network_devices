package pl.project.connection;
import java.io.IOException;

/**
 * @author Konrad
 * date 18/02/2019
 * project_name Backup_Network_Devices
 */
public interface IConnection {
    boolean connect() throws IOException;
}
