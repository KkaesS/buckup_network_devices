package pl.project.connection;

/**
 * @author Konrad
 * date 24/01/2019
 * project_name Backup_Network_Devices
 */
public class IOS implements IConfiguration {
    @Override
    public String backup() {
            return "copy flash: tftp:";
    }

    @Override
    public String restore() {
        return "copy tftp: flash:";
    }
}
