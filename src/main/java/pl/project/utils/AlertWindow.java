package pl.project.utils;

import com.jfoenix.controls.JFXCheckBox;
import javafx.scene.control.*;
import javafx.scene.layout.Region;

import java.util.Optional;

/**
 * @author Konrad
 * date 12/01/2019
 * project_name Backup_Network_Devices
 */
public class AlertWindow {

    private AlertWindow() {
        throw new IllegalStateException("Utility class");
    }

    public static void information(String header, String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }

    public static Optional<ButtonType> confirmation(String header, String title, String contentText) {

        ButtonType yes = new ButtonType("Tak", ButtonBar.ButtonData.YES);
        ButtonType no = new ButtonType("Nie", ButtonBar.ButtonData.NO);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", yes, no);
        alert.setHeaderText(header);
        alert.setContentText(contentText);
        alert.setTitle(title);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        return alert.showAndWait();
    }

    public static String backupRestoreIOS(String header, String title) {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText("Podaj nazwę pliku:");

        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("Anuluj");

        JFXCheckBox checkBox = new JFXCheckBox("Z pliku?");
        dialog.setGraphic(checkBox);
        TextField textField = dialog.getEditor();

        textField.disableProperty().bind(checkBox.selectedProperty().not());

        Optional<String> result = dialog.showAndWait();
        return result.orElse("_null_");
    }

    public static String iOS(String header, String title) {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText("Podaj nazwę pliku:");

        Button ok = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("Anuluj");

        TextField textField = dialog.getEditor();
        ok.disableProperty().bind(textField.textProperty().isEmpty());

        Optional<String> result = dialog.showAndWait();
        return result.orElse("_null_");
    }

    public static Optional<ButtonType> warring(String header, String title) {

        ButtonType yes = new ButtonType("Tak", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("Nie", ButtonBar.ButtonData.NO);

        Alert alert = new Alert(Alert.AlertType.WARNING, "", yes, no);
        alert.setHeaderText(header);
        alert.setTitle(title);
        return alert.showAndWait();
    }

    public static void error(String header, String title) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }

}
