package pl.project.utils;

import javafx.collections.ObservableList;
import pl.project.entity.Device;
import pl.project.entity.ListOfDevices;
import pl.project.modelfx.ListOfDevicesFx;

/**
 * @author Konrad
 * date 03/01/2019
 * project_name Backup_Network_Devices
 */
public class ListOfDevicesConverter {

    private ListOfDevicesConverter() {
        throw new IllegalStateException("Utility class");
    }

    public static ListOfDevices convertToListOfDevices(ListOfDevicesFx listOfDevicesFx) {
        ListOfDevices listOfDevices = new ListOfDevices();
        listOfDevices.setUser(listOfDevicesFx.getUser());
        listOfDevices.setId(listOfDevicesFx.getId());
        listOfDevices.setName(listOfDevicesFx.getName());
        listOfDevices.setDevices(listOfDevicesFx.getDeviceList());

        return listOfDevices;
    }

    public static ListOfDevicesFx convertToListOfDevicesFx(ListOfDevices listOfDevices) {
        ListOfDevicesFx listOfDevicesFx = new ListOfDevicesFx();
        listOfDevicesFx.setUser(listOfDevices.getUser());
        listOfDevicesFx.setId(listOfDevices.getId());
        listOfDevicesFx.setName(listOfDevices.getName());
        return listOfDevicesFx;
    }
}
