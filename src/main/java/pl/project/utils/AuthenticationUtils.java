package pl.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.project.entity.User;
import pl.project.modelfx.UserModel;
import pl.project.service.user.UserService;

/**
 * @author Konrad
 * date 28/12/2018
 * project_name Backup_Network_Devices
 */

@Component
public class AuthenticationUtils {
    private UserService userService;

    @Autowired
    public AuthenticationUtils(UserService userService, UserModel userModel) {
        this.userService = userService;
    }

    public User getAuthenticatedUser() {
        return userService.getUser(getAuthenticatedUsername());
    }

    private String getAuthenticatedUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication.getName();
        }
        return null;
    }

    public static boolean isAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && !(auth instanceof AnonymousAuthenticationToken) && auth.isAuthenticated();
    }

    public static void initSecurity() {
        SecurityContextHolder.setStrategyName("MODE_GLOBAL");
        initAnonymous();
    }

    public static void initAnonymous() {
        AnonymousAuthenticationToken auth = new AnonymousAuthenticationToken("anonymous", "anonymous",
                AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public static void logout() {
        SecurityContextHolder.clearContext();
        initAnonymous();
    }
}
