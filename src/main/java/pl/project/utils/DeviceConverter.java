package pl.project.utils;

import pl.project.entity.Device;
import pl.project.modelfx.DeviceFx;

/**
 * @author Konrad
 * date 14/12/2018
 * project_name Backup_Network_Devices
 */
public class DeviceConverter {

    private DeviceConverter() {
        throw new IllegalStateException("Utility class");
    }

    public static Device convertToDevice(DeviceFx deviceFx) {
        Device device = new Device();
        device.setId(deviceFx.getId());
        device.setName(deviceFx.getName());
        device.setIp(deviceFx.getIp());
        device.setPasswordVty(deviceFx.getPasswordVty());
        device.setPassword(deviceFx.getPassword());
        device.setUser(deviceFx.getUser());
        device.setTftp(deviceFx.getTftp());
        device.setPasswordSSH(deviceFx.getPasswordSSH());
        device.setLoginSSH(deviceFx.getLoginSSH());
        device.setConnectionType(deviceFx.getConnectionType());
        return device;
    }

    public static DeviceFx convertToDeviceFx(Device device) {
        DeviceFx deviceFx = new DeviceFx();
        deviceFx.setId(device.getId());
        deviceFx.setIp(device.getIp());
        deviceFx.setName(device.getName());
        deviceFx.setPasswordVty(device.getPasswordVty());
        deviceFx.setPassword(device.getPassword());
        deviceFx.setTftp(device.getTftp());
        deviceFx.setUser(device.getUser());
        deviceFx.setPasswordSSH(device.getPasswordSSH());
        deviceFx.setLoginSSH(device.getLoginSSH());
        deviceFx.setConnectionType(device.getConnectionType());
        return deviceFx;
    }
}
