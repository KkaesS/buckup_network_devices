package pl.project.utils;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.modelfx.UserModel;

/**
 * @author Konrad
 * date 27/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class WindowUtils {

    private double xOffset = 0;
    private double yOffset = 0;
    private static UserModel userModel;

    @Autowired
    public WindowUtils(UserModel userModel) {
        WindowUtils.userModel = userModel;
    }

    public void onPressed(Parent root) {
        root.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });
    }

    public void onDragged(Stage stage, Parent root) {
        root.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset);
            stage.setY(event.getScreenY() - yOffset);
        });
    }

    public static void closeView(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.close();
    }

    public static void minimizeView(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    public static void exitApp() {
        Task task = new Task() {
            @Override
            protected Object call() {
                userModel.deleteAnonymousFields();
                return null;
            }
        };
        new Thread(task).start();

        Platform.exit();
    }
}
