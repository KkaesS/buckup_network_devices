package pl.project.utils;

import java.util.Base64;

/**
 * @author Konrad
 * date 20/01/2019
 * project_name Backup_Network_Devices
 */

public class PasswordCrypt {

    private PasswordCrypt() {
        throw new IllegalStateException("Utility class");
    }

    public static String encrypt(String password) {
        return Base64.getEncoder().withoutPadding().encodeToString(password.getBytes());
    }

    public static String decrypt(String encrypt) {
        byte[] decodeByte = Base64.getDecoder().decode(encrypt.getBytes());
        return new String(decodeByte);
    }
}
