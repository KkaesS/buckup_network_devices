package pl.project.utils;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author Konrad
 * date 09/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class FxmlUtils {

    private AnnotationConfigApplicationContext context;

    private WindowUtils windowUtils;

    @Autowired
    FxmlUtils(AnnotationConfigApplicationContext context, WindowUtils windowUtils) {
        this.context = context;
        this.windowUtils = windowUtils;
    }


    private void showStage(String path ) throws IOException {
        FXMLLoader loader = getLoader(path);
        Parent root = loader.load();

        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.initStyle(StageStyle.UNDECORATED);
            stage.initModality(Modality.APPLICATION_MODAL);

            windowUtils.onPressed(root);
            windowUtils.onDragged(stage, root);

            stage.setScene(new Scene(root));
            stage.show();
        });

    }

    public void openView(String path) {
        try {
            showStage(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public FXMLLoader getLoader(String path) {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(context::getBean);
        loader.setLocation(getClass().getResource(path));
        return loader;
    }

}

