package pl.project.utils;

import pl.project.entity.Authorities;
import pl.project.modelfx.AuthoritiesFx;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
public class AuthoritiesConverter {

    public static Authorities convertToAuthorities(AuthoritiesFx authoritiesFx) {
        Authorities authorities = new Authorities();
        authorities.setAuthority(authoritiesFx.getAuthority());
        authorities.setUser(authoritiesFx.getUser());
        return authorities;
    }
}
