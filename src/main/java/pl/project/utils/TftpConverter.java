package pl.project.utils;

import pl.project.entity.Tftp;
import pl.project.modelfx.TftpFx;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */
public class TftpConverter {

    private TftpConverter() {
        throw new IllegalStateException("Utility class");
    }

    public static TftpFx convertToTftpFx(Tftp tftp) {
        TftpFx tftpFx = new TftpFx();
        tftpFx.setId(tftp.getId());
        tftpFx.setIp(tftp.getIp());
        tftpFx.setName(tftp.getName());
        tftpFx.setUser(tftp.getUser());
        return tftpFx;
    }

    public static Tftp convertToTftp(TftpFx tftpFx) {
        Tftp tftp = new Tftp();
        tftp.setId(tftpFx.getId());
        tftp.setIp(tftpFx.getIp());
        tftp.setUser(tftpFx.getUser());
        tftp.setName(tftpFx.getName());
        return tftp;
    }


}
