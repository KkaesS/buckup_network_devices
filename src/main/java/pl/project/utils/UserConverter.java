package pl.project.utils;

import pl.project.entity.User;
import pl.project.modelfx.UserFx;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
public class UserConverter {

    private UserConverter() {
        throw new IllegalStateException("Utility class");
    }

    public static User convertToUser(UserFx userFx) {
        User user = new User();
        user.setUsername(userFx.getUsername());
        user.setEnabled(1);
        user.setPassword(userFx.getPassword());
        return user;
    }
}
