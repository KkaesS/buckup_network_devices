package pl.project.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */

@Entity
@Table(name = "devices")
public class Device implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "ip")
    private String ip;

    @Column(name = "password_vty")
    private String passwordVty;

    @Column(name = "password")
    private String password;

    @Column(name = "passwordSSH")
    private String passwordSSH;

    @Column(name = "loginSSH")
    private String loginSSH;

    @Column(name = "connectionType")
    private String connectionType;

    // deleting ONLY devices, NOT user
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.DETACH,
            CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "username")
    private User user;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH,
            CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "list_of_devices_has_devices",
            joinColumns = @JoinColumn(name = "device_id"),
            inverseJoinColumns = @JoinColumn(name = "list_id"))
    private List<ListOfDevices> listOfDevices;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "tftp_server_id")
    private Tftp tftp;

    public Device() {
        this("Not set", "Not set", null);
    }

    public Device(String ip, String name) {
        this.ip = ip;
        this.name = name;
    }

    public Device(String name, String ip, User user) {
        this.name = name;
        this.ip = ip;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ListOfDevices> getListOfDevices() {
        return listOfDevices;
    }

    public String getPasswordVty() {
        return passwordVty;
    }

    public void setPasswordVty(String login) {
        this.passwordVty = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setListOfDevices(List<ListOfDevices> listOfDevices) {
        this.listOfDevices = listOfDevices;
    }

    public Tftp getTftp() {
        return tftp;
    }

    public void setTftp(Tftp tftp) {
        this.tftp = tftp;
    }

    public String getPasswordSSH() {
        return passwordSSH;
    }

    public void setPasswordSSH(String passwordSSH) {
        this.passwordSSH = passwordSSH;
    }

    public String getLoginSSH() {
        return loginSSH;
    }

    public void setLoginSSH(String loginSSH) {
        this.loginSSH = loginSSH;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;
        Device device = (Device) o;
        return id == device.id &&
                Objects.equals(name, device.name) &&
                Objects.equals(ip, device.ip) &&
                Objects.equals(user, device.user) &&
                Objects.equals(listOfDevices, device.listOfDevices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, ip, user, listOfDevices);
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", user=" + user +
                '}';
    }
}
