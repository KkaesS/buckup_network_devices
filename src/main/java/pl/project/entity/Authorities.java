package pl.project.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Konrad
 * date 20/12/2018
 * project_name Backup_Network_Devices
 */

@Entity
@Table(name = "authorities")
public class Authorities implements Serializable {

    @Id
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH,
            CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "username")
    private User user;

    @Column(name = "authority")
    private String authority;

    public Authorities() {
        this(new User(), "Not set");
    }

    public Authorities(User user, String authority) {
        this.user = user;
        this.authority = authority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
