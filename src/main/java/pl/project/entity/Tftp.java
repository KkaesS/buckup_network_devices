package pl.project.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */
@Entity
@Table(name = "tftp")
public class Tftp implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "ip")
    private String ip;

    @OneToMany(mappedBy = "tftp",fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST})
    private List<Device> devices;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.DETACH,
            CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "username")
    private User user;

    public Tftp() {
        this("Not set", "Not set");
    }

    public Tftp(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}

