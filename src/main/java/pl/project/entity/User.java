package pl.project.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private int enabled;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Device> userDevices;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.DETACH,
            CascadeType.MERGE, CascadeType.REFRESH})
    private List<Authorities> authoritiesList;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ListOfDevices> listOfDevices;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tftp> tftpList;

    public User() {
        this("Not set", "Not set", 0);
    }

    public User(String username, String password,int enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public List<ListOfDevices> getListOfDevices() {
        return listOfDevices;
    }

    public void setListOfDevices(List<ListOfDevices> listOfDevices) {
        this.listOfDevices = listOfDevices;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public List<Device> getUserDevices() {
        return userDevices;
    }

    public void setUserDevices(List<Device> userDevices) {
        this.userDevices = userDevices;
    }

    public List<Authorities> getAuthoritiesList() {
        return authoritiesList;
    }

    public void setAuthoritiesList(List<Authorities> authoritiesList) {
        this.authoritiesList = authoritiesList;
    }

    public List<Tftp> getTftpList() {
        return tftpList;
    }

    public void setTftpList(List<Tftp> tftpList) {
        this.tftpList = tftpList;
    }

    @Override
    public String toString() {
        return username;
    }
}
