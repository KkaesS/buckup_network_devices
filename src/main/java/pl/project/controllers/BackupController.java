package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import pl.project.modelfx.DeviceFx;
import pl.project.modelfx.DeviceModel;
import pl.project.connection.*;
import pl.project.utils.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Konrad
 * date 09/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class BackupController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private static final String ADD_DEVICE = "/view/backup/addDeviceView.fxml";
    private static final String SAVE_DEVICE_LIST = "/view/backup/saveDeviceList.fxml";
    private static final String READ_FROM_LIST = "/view/backup/readFromList.fxml";
    private static final String SETTINGS = "/view/tftpView.fxml";
    private static final String LOGIN_VIEW = "/view/main/loginView.fxml";
    private static final String ABOUT_VIEW = "/view/aboutView.fxml";


    private FxmlUtils fxmlUtils;

    MainController mainController;

    DeviceModel deviceModel;

    @FXML
    private TableView<DeviceFx> deviceTableView;

    @FXML
    private TableColumn<DeviceFx, String> deviceNameColumn;

    @FXML
    private TableColumn<DeviceFx, String> deviceIpColumn;

    @FXML
    private TableColumn<DeviceFx, Boolean> selectColumn;

    @FXML
    private HBox viewAllHBox;

    @FXML
    private HBox readFromListHBox;

    @FXML
    private HBox restoreHBox;

    @FXML
    private HBox backupHBox;

    @FXML
    private HBox newListHBox;

    @FXML
    private ProgressIndicator spinner;

    @FXML
    private JFXButton loginButton;

    @FXML
    private JFXButton cancelButton;

    @FXML
    private JFXComboBox<Configuration> restoreCombobox;

    @FXML
    private JFXComboBox<Configuration> backupCombobox;

    private BackupRestoreDevice device;

    @Autowired
    BackupController(FxmlUtils fxmlUtils, DeviceModel deviceModel, MainController main) {
        this.fxmlUtils = fxmlUtils;
        this.deviceModel = deviceModel;
        this.mainController = main;
    }

    private enum BackupOrRestoreType {
        BACKUP,
        RESTORE
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("initialize BackupController");
        backupCombobox.getItems().addAll(Configuration.values());
        restoreCombobox.getItems().addAll(Configuration.values());

        init();
    }

    private void init() {

        System.out.println("authenticated? " + AuthenticationUtils.isAuthenticated());

        deviceTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        deviceTableView.setPlaceholder(new Label("Brak dostępnych urządzeń."));

        setTableView(deviceModel.getAllUserDevices());

        // add a checkbox to TableView and listen the checkbox values
        listenCheckbox();

        selectColumn.setCellValueFactory(param -> param.getValue().selectProperty());

        // set an editable table
        deviceNameColumn.setCellFactory(
                TextFieldTableCell.forTableColumn());
        deviceIpColumn.setCellFactory(
                TextFieldTableCell.forTableColumn());

        // if the user has edited fields in the table ...
        deviceTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                deviceModel.setDeviceEdit(newValue));

        // only for the user
        boolean authenticated = AuthenticationUtils.isAuthenticated();
        newListHBox.setVisible(authenticated);
        readFromListHBox.setVisible(authenticated);
        viewAllHBox.setVisible(authenticated);

        if (AuthenticationUtils.isAuthenticated())
            loginButton.setText("Wyloguj");
        else {
            loginButton.setText("Zaloguj");
        }

        // disable the restoring and the backup labels at startup
        setDisableHBox(true);
        setVisibleSpinner(false);
        deviceTableView.setDisable(false);
    }

    @FXML
    void login(ActionEvent event) {
        if (AuthenticationUtils.isAuthenticated()) {
            Optional<ButtonType> buttonType = AlertWindow.warring("Czy na pewno chcesz się wylogować?", "Uwaga!");
            if (buttonType.isPresent() && buttonType.get().getButtonData().getTypeCode().equals("O")) {

                AuthenticationUtils.logout();
                WindowUtils.closeView(event);
                mainController.getLoginButton().setText("Zaloguj");
                AlertWindow.information("Zostałeś poprawnie wylogowany.", "Wylogowano");
                deviceModel.getDeviceObservableList().clear();
            }
        } else {
            Optional<ButtonType> buttonType = AlertWindow.warring("Czy na pewno chcesz się zalogować? \nTwoje dane zostaną niezapisane.", "Uwaga!");
            if (buttonType.isPresent() && buttonType.get().getButtonData().getTypeCode().equals("O")) {
                WindowUtils.closeView(event);
                fxmlUtils.openView(LOGIN_VIEW);
            }
        }
    }

    @FXML
    void cancel() {
        init();
        if (device.getTelnet() != null) {
            device.getTelnet().disconnect();
            System.out.println("Telnet disconnect");
        } else if (device.getSsh() != null) device.getSsh().disconnect();
    }

    @FXML
    void about() {
        fxmlUtils.openView(ABOUT_VIEW);
    }

    @FXML
    void exitMenuItem() {
        Optional<ButtonType> buttonType = AlertWindow.confirmation("Czy na pewno chcesz wyjść?", "Uwaga!", "");

        if (buttonType.isPresent() && buttonType.get().getButtonData().getTypeCode().equals("Y")) {
            spinner.setVisible(true);
            WindowUtils.exitApp();
            spinner.setVisible(false);
        }
    }

    @FXML
    void settings() {
        fxmlUtils.openView(SETTINGS);
    }

    @FXML
    void restore() {
        log.info("restore");
        String value = "";
        String title = restoreCombobox.getSelectionModel().getSelectedItem().name();

        if (!restoreCombobox.getSelectionModel().getSelectedItem().equals(Configuration.WYBIERZ)) {
            String header = "Przywróć domyślną konfigurację lub wprowadź nazwę pliku konfiguracyjnego z serwera TFTP";

            if (restoreCombobox.getSelectionModel().getSelectedItem().equals(Configuration.STARTUP_CONFIG)) {
                value = AlertWindow.backupRestoreIOS(header, "Przywróć " + title);

            } else if (restoreCombobox.getSelectionModel().getSelectedItem().equals(Configuration.IOS)) {
                header = "Wprowadź nazwę pliku systemu operacyjnego Cisco IOS";
                value = AlertWindow.iOS(header, "Przywróć " + title);

            } else if (restoreCombobox.getSelectionModel().getSelectedItem().equals(Configuration.RUNNING_CONFIG))
                value = AlertWindow.backupRestoreIOS(header, "Przywróć " + title);

            if (!value.equals("_null_"))
                backupOrRestore(BackupOrRestoreType.RESTORE, value);

        }
    }

    @FXML
    void backup() {
        log.info("backup");
        String title = backupCombobox.getSelectionModel().getSelectedItem().name();
        if (!backupCombobox.getSelectionModel().getSelectedItem().equals(Configuration.WYBIERZ)) {
            String header = "Wykonać backup pliku konfiguracyjnego " + title + "?";

            if (backupCombobox.getSelectionModel().getSelectedItem().equals(Configuration.STARTUP_CONFIG)) {
                String txt = "Uwaga! \n1.Startup-config zostanie skopiowany do running-config" +
                        "\n2.Powstały plik zostanie nadpisany, jeżeli na serwerze TFTP znajduje się plik o takiej samej nazwie" +
                        "\n\nKontynuować?";
                Optional<ButtonType> alert = AlertWindow.confirmation(header, "Backup " + title, txt);

                if (alert.isPresent() && alert.get().getButtonData().getTypeCode().equals("Y"))
                    backupOrRestore(BackupOrRestoreType.BACKUP, "");

            } else if (backupCombobox.getSelectionModel().getSelectedItem().equals(Configuration.RUNNING_CONFIG)) {
                String content = "Uwaga\nPowstały plik zostanie nadpisany, jeżeli na serwerze TFTP znajduje się plik o takiej samej nazwie" +
                        "\n\nKontynuować?";
                Optional<ButtonType> alert = AlertWindow.confirmation(header, "Backup " + title, content);
                if (alert.isPresent() && alert.get().getButtonData().getTypeCode().equals("Y"))
                    backupOrRestore(BackupOrRestoreType.BACKUP, "");

            } else if (backupCombobox.getSelectionModel().getSelectedItem().equals(Configuration.IOS)) {
                String fn = AlertWindow.iOS("Wprowadź nazwę pliku systemu operacyjnego Cisco IOS"
                        , "Backup " + title);
                if (!fn.equals("_null_"))
                    backupOrRestore(BackupOrRestoreType.BACKUP, fn);
            }
        }
    }

    @FXML
    void deleteDevice() {
        Optional<ButtonType> alert = AlertWindow.warring("Czy na pewno chcesz usunąć urządzenie?", "Uwaga");
        if (alert.isPresent() && alert.get().getButtonData().getTypeCode().equals("O")){
            deviceModel.deleteDevice(deviceTableView.getSelectionModel().getSelectedItem());
            setTableView(deviceModel.getAllUserDevices());
        }
    }

    private void backupOrRestore(BackupOrRestoreType backupOrRestoreType, String file) {
        Task task = new Task<Void>() {
            @Override
            protected Void call() {
                setControls(true);
                boolean result = false;

                String passwordSSH = null;
                String loginSSH = null;
                String filename = null;

                // e.g Run_conf
                Configuration configuration = getBackupOrRestore(backupOrRestoreType);
                // e.g OBJECT Run_conf
                device = getConfiguration(configuration);

                for (DeviceFx deviceFx : deviceModel.getSelectedList()) {

                    if (file.equals(""))
                        filename = deviceFx.getName();
                    else filename = file;

                    assert configuration != null;
                    if (!configuration.name().equals("IOS") && file.equals(""))
                        filename += configuration.name();

                    System.out.println("FILE " + filename);

                    String ip = deviceFx.getIp();
                    String ipTftp = deviceFx.getTftp().getIp();
                    String passwordVty = PasswordCrypt.decrypt(deviceFx.getPasswordVty());
                    String password = PasswordCrypt.decrypt(deviceFx.getPassword());
                    String connectionType = deviceFx.getConnectionType();

                    if (deviceFx.getPasswordSSH() != null)
                        passwordSSH = PasswordCrypt.decrypt(deviceFx.getPasswordSSH());

                    if (deviceFx.getLoginSSH() != null)
                        loginSSH = PasswordCrypt.decrypt(deviceFx.getLoginSSH());

                    System.out.println(passwordSSH + " " + loginSSH);
                    System.out.println(connectionType);
                    log.info(filename + " " + ip + " " + ipTftp + " " + passwordVty + " " + password + " " + deviceFx.getPassword() + " " + backupCombobox.getSelectionModel().getSelectedItem());


                    result = doBackupOrRestore(device, ip, filename, ipTftp, passwordVty, password, loginSSH, passwordSSH, setConnection(connectionType), backupOrRestoreType);
                }

                checkResult(result);
                setControls(false);
                return null;
            }
        };

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    private IConnection setConnection(String connection) {
        if (connection.equals("Telnet")) {
            return new Telnet();
        } else if (connection.equals("SSH"))
            return new SSH();
        return null;
    }

    private Configuration getBackupOrRestore(BackupOrRestoreType type) {
        Optional<BackupOrRestoreType> optional = Optional.ofNullable(type);

        if (optional.isPresent() && type.equals(BackupOrRestoreType.BACKUP))
            return backupCombobox.getSelectionModel().getSelectedItem();
        else if (optional.isPresent() && type.equals(BackupOrRestoreType.RESTORE))
            return restoreCombobox.getSelectionModel().getSelectedItem();
        return null;
    }

    private BackupRestoreDevice getConfiguration(Configuration configuration) {
        Optional<Configuration> optional = Optional.ofNullable(configuration);

        if (optional.isPresent() && configuration.equals(Configuration.RUNNING_CONFIG))
            return new BackupRestoreDevice(new RunningConfig());
        else if (optional.isPresent() && configuration.equals(Configuration.STARTUP_CONFIG))
            return new BackupRestoreDevice(new StartupConfig());
        else if (optional.isPresent() && configuration.equals(Configuration.IOS))
            return new BackupRestoreDevice(new IOS());
        return null;
    }

    private boolean doBackupOrRestore(BackupRestoreDevice device, String ip, String filename, String ipTftp, String login,
                                      String passwordEnable, String loginSSH, String passwordSSH, IConnection connection, BackupOrRestoreType backupOrRestoreType) {
        boolean result = false;
        log.info("doBackupOrRestore");
        if (backupOrRestoreType.equals(BackupOrRestoreType.BACKUP)) {
            try {
                result = device.backup(ip, filename, ipTftp, login, passwordEnable, loginSSH, passwordSSH, connection);
            } catch (IOException e) {
                Platform.runLater(() -> AlertWindow.error("Sprawdź połączenie z urządzeniem lub z serwerem TFTP", e.getMessage()));
            }
        } else if (backupOrRestoreType.equals(BackupOrRestoreType.RESTORE)) {
            try {
                result = device.restore(ip, filename, ipTftp, login, passwordEnable, loginSSH, passwordSSH, connection);
            } catch (IOException e) {
                Platform.runLater(() -> AlertWindow.error("Sprawdź połączenie z urządzeniem lub z serwerem TFTP", e.getMessage()));
            }
        }
        return result;
    }

    private void checkResult(boolean value) {
        if (value)
            Platform.runLater(() -> AlertWindow.information("Operacja przebiegła pomyślnie", "Sukces!"));
    }

    private void setControls(boolean value) {
        setVisibleSpinner(value);
        setDisableHBox(value);
        deviceTableView.setDisable(value);
    }

    public void listenCheckbox() {
        // add a checkbox to TableView and listen the checkbox values
        deviceModel.getSelectedList().clear();

        selectColumn.setCellFactory(CheckBoxTableCell.forTableColumn(param -> {

            DeviceFx deviceFx = deviceTableView.getItems().get(param);

            //  listen, if the user has unchecked or checked the checkbox, then the device in list will be removed or added
            if (deviceTableView.getItems().get(param).selectProperty().getValue())
                deviceModel.addSelectedDevice(deviceFx);
            else
                deviceModel.removeSelectedDevice(deviceFx);

            if (deviceModel.getSelectedList().isEmpty()) {
                setDisableHBox(true);
                newListHBox.setVisible(false);
            } else {
                setDisableHBox(false);

                if (AuthenticationUtils.isAuthenticated())
                    newListHBox.setVisible(true);
            }
            return deviceTableView.getItems().get(param).selectProperty();
        }));
    }


    @FXML
    void viewAllUserDevices() {
        setTableView(deviceModel.getAllUserDevices());
    }

    @FXML
    void editDevice() {
        // get clicked devices (object DeviceFX) and save them to the Collections
        Optional<DeviceFx> isSelected = Optional.ofNullable(deviceTableView.getSelectionModel().getSelectedItem());
        isSelected.ifPresent(deviceFx -> deviceModel.setDeviceEdit(deviceFx));
        fxmlUtils.openView(ADD_DEVICE);
    }

    @FXML
    void editIpColumn(TableColumn.CellEditEvent<DeviceFx, String> editEvent) {
        Optional<String> isSelected = Optional.ofNullable(editEvent.getNewValue());
        if (isSelected.isPresent() && isValidIp(isSelected.get())) {
            deviceModel.getDeviceEdit().setIp(isSelected.get());
            deviceModel.saveEditDevice();
        } else {
            AlertWindow.error("Nieprawidłowy format adresu IP !", "Błąd");
            setTableView(deviceModel.getAllUserDevices());
        }

    }

    @FXML
    void editNameColumn(TableColumn.CellEditEvent<DeviceFx, String> editEvent) {
        Optional<String> isSelected = Optional.ofNullable(editEvent.getNewValue());
        if (isSelected.isPresent()) {
            deviceModel.getDeviceEdit().setName(isSelected.get());
            deviceModel.saveEditDevice();
        }
    }

    @FXML
    void addDevice() {
        fxmlUtils.openView(ADD_DEVICE);
    }

    @FXML
    void saveDevicesListWindow() {
        try {
            fxmlUtils.openView(SAVE_DEVICE_LIST);
        } catch (AccessDeniedException ex) {
            log.info("ONLY USER");
        }

    }

    @FXML
    void readFromList() {
        fxmlUtils.openView(READ_FROM_LIST);
    }

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    void minimize(ActionEvent event) {
        WindowUtils.minimizeView(event);
    }

    void setTableView(@NotNull ObservableList<DeviceFx> deviceFxObservableList) {
        log.info("setTableView");
        deviceTableView.setItems(deviceFxObservableList);
        deviceNameColumn.setCellValueFactory(param -> param.getValue().nameProperty());
        deviceIpColumn.setCellValueFactory(param -> param.getValue().ipProperty());
    }

    private void setDisableHBox(boolean value) {
        backupHBox.setDisable(value);
        restoreHBox.setDisable(value);
    }

    private void setVisibleSpinner(boolean value) {
        spinner.setVisible(value);
        cancelButton.setVisible(value);
    }

    private boolean isValidIp(String address) {
        Pattern pattern = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
        Matcher matcher = pattern.matcher(address);
        return matcher.find();
    }

}
