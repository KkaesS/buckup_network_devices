package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.project.modelfx.UserModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.FxmlUtils;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 14/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class LoginController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private static final String REGISTER_VIEW = "/view/main/registerView.fxml";

    @FXML
    private JFXTextField usernameTextField;

    @FXML
    private JFXPasswordField passwordTextField;

    @FXML
    private JFXButton loginButton;

    private AuthenticationManager authenticationManager;

    private FxmlUtils fxmlUtils;

    @Autowired
    MainController mainController;

    private UserModel userModel;

    @Autowired
    LoginController(AuthenticationManager auth, FxmlUtils fxmlUtils, UserModel userModel) {
        this.authenticationManager = auth;
        this.fxmlUtils = fxmlUtils;
        this.userModel = userModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginButton.disableProperty().bind(passwordTextField.textProperty().isEmpty()
                        .or(usernameTextField.textProperty().isEmpty()));
    }

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    void login(ActionEvent event) {

        UsernamePasswordAuthenticationToken authReq =
                new UsernamePasswordAuthenticationToken(usernameTextField.getText(), passwordTextField.getText());

        try {
            Authentication authentication = authenticationManager.authenticate(authReq);
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);

            // close the view when the user is LOGGED IN
            AlertWindow.information("Zostałeś pomyślnie zalogowany", "Zalogowano");
            userModel.deleteAnonymousFields();
            WindowUtils.closeView(event);
            mainController.getLoginButton().setText("Wyloguj");

        } catch (AuthenticationException ex) {
            AlertWindow.error("Podany login i/lub hasło są nieprawidłowe. Spróbuj jeszcze raz.", ex.getMessage());
        }
    }

    @FXML
    public void registerView() {
        fxmlUtils.openView(REGISTER_VIEW);
    }


}