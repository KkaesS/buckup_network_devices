package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.project.modelfx.AuthoritiesFx;
import pl.project.modelfx.AuthoritiesModel;
import pl.project.modelfx.UserFx;
import pl.project.modelfx.UserModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.UserConverter;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Konrad
 * date 16/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class RegisterController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    @FXML
    private JFXTextField usernameTextField;

    @FXML
    private JFXPasswordField repeatPass;

    @FXML
    private JFXPasswordField passwordTextField;

    @FXML
    private JFXButton registerButton;

    @FXML
    private Text emailText;

    private UserModel userModel;

    private AuthoritiesModel authoritiesModel;

    @Autowired
    RegisterController(UserModel user, AuthoritiesModel auth) {
        this.userModel = user;
        this.authoritiesModel = auth;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        emailText.setVisible(false);
        registerButton.setDisable(true);

        repeatPass.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.equals(passwordTextField.getText()))
                registerButton.setDisable(true);
            else
                registerButton.setDisable(false);
        });
    }

    @FXML
    void register(ActionEvent event) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String username = usernameTextField.getText();
        String password = passwordEncoder.encode(passwordTextField.getText());

        UserFx userFx = new UserFx(username, password, 1);
        AuthoritiesFx authoritiesFx = new AuthoritiesFx(UserConverter.convertToUser(userFx), "USER");

        Task task = new Task() {
            @Override
            protected Object call() {
                userModel.saveUser(userFx);
                authoritiesModel.saveAuthority(authoritiesFx);
                return null;
            }
        };

        new Thread(task).start();

        AlertWindow.information(username.toUpperCase() + " " + "został pomyślnie zarejestrowany!", "Rejestracja");
        WindowUtils.closeView(event);


    }

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }

}
