package pl.project.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import org.springframework.stereotype.Component;
import pl.project.utils.WindowUtils;

/**
 * @author Konrad
 * date 26/02/2019
 * project_name Backup_Network_Devices
 */
@Component
public class AboutController {

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }
}
