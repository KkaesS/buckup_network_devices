package pl.project.controllers;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.modelfx.DeviceFx;
import pl.project.modelfx.DeviceModel;
import pl.project.modelfx.TftpFx;
import pl.project.modelfx.TftpModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.PasswordCrypt;
import pl.project.utils.TftpConverter;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Konrad
 * date 13/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class AddDeviceController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    @FXML
    private JFXTextField ipTextField;

    @FXML
    private JFXTextField nameDeviceTextField;

    @FXML
    private JFXButton addDeviceButton;

    @FXML
    private JFXPasswordField passwordVtyDevTextField;

    @FXML
    private JFXPasswordField passwordDevTextFiled;

    @FXML
    private JFXButton exitButton;

    @FXML
    private JFXComboBox<TftpFx> tftpServerComboBox;

    @FXML
    private ToggleGroup radioButton;

    @FXML
    private HBox passwordVTYHbox;

    @FXML
    private JFXCheckBox isUserCheckBox;

    @FXML
    private HBox usernameSSHTelnetHbox;

    @FXML
    private JFXTextField usernameSSHTelnetTextField;

    @FXML
    private HBox passwordSSHHbox;

    @FXML
    private JFXPasswordField passwordSSHTextField;

    @FXML
    private VBox radioButtonVbox;

    @FXML
    private JFXRadioButton telnetRadioButton;

    @FXML
    private JFXRadioButton sshRadioButton;

    private DeviceModel deviceModel;

    private TftpModel tftpModel;

    private BackupController backupController;

    private String oldIP;

    private String selectedConnection;

    @Autowired
    AddDeviceController(DeviceModel deviceModel, TftpModel tftpModel, BackupController backupController) {
        this.deviceModel = deviceModel;
        this.tftpModel = tftpModel;
        this.backupController = backupController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initBindings();
        passwordSSHHbox.setDisable(true);
        usernameSSHTelnetHbox.setDisable(true);
        passwordVTYHbox.setDisable(true);
        isUserCheckBox.setDisable(true);

        tftpServerComboBox.getItems().addAll(tftpModel.getUserTftpServerList());
        telnetOrSSHBinding();
        editDeviceSetField();

        deviceModel.setDeviceEdit(null);
    }

    private void editDeviceSetField() {
        if (deviceModel.getDeviceEdit() != null) {

            System.out.println("getDeviceEdit");
            ipTextField.setText(deviceModel.getDeviceEdit().getIp());
            nameDeviceTextField.setText(deviceModel.getDeviceEdit().getName());
            tftpServerComboBox.getItems().stream()
                    .filter(server -> deviceModel.getDeviceEdit().getTftp().getId() == server.getId())
                    .findAny()
                    .ifPresent(tftpServerComboBox.getSelectionModel()::select);
            passwordVtyDevTextField.setText(PasswordCrypt.decrypt(deviceModel.getDeviceEdit().getPasswordVty()));

            passwordDevTextFiled.setText(PasswordCrypt.decrypt(deviceModel.getDeviceEdit().getPassword()));
            oldIP = deviceModel.getDeviceEdit().getIp();

            if (deviceModel.getDeviceEdit().getConnectionType().equals("Telnet")) {
                radioButton.selectToggle(telnetRadioButton);
                if (deviceModel.getDeviceEdit().getLoginSSH() != null) {
                    usernameSSHTelnetTextField.setText(PasswordCrypt.decrypt(deviceModel.getDeviceEdit().getLoginSSH()));
                    isUserCheckBox.setSelected(true);
                }
            } else {
                radioButton.selectToggle(sshRadioButton);
                passwordSSHTextField.setText(PasswordCrypt.decrypt(deviceModel.getDeviceEdit().getPasswordSSH()));
                usernameSSHTelnetTextField.setText(PasswordCrypt.decrypt(deviceModel.getDeviceEdit().getLoginSSH()));
            }
        }
    }

    private void telnetOrSSHBinding() {
            radioButton.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
                RadioButton r = (RadioButton) newValue.getToggleGroup().getSelectedToggle();
                selectedConnection = r.getText();

                if (selectedConnection.equals("Telnet")) {
                    radioButtonsBinding(true, passwordVtyDevTextField.textProperty().isEmpty());
                    usernameSSHTelnetTextField.clear();
                    passwordSSHTextField.clear();

                    isUserCheckBox.selectedProperty().addListener((observable1, oldValue1, newValue1) -> {
                        if (newValue1) {
                            usernameSSHTelnetHbox.setDisable(false);
                            addDeviceButton.disableProperty().bind(usernameSSHTelnetTextField.textProperty().isEmpty()
                                    .or(passwordVtyDevTextField.textProperty().isEmpty()));
                        } else {
                            usernameSSHTelnetHbox.setDisable(true);
                            usernameSSHTelnetTextField.clear();
                            addDeviceButton.disableProperty().bind(passwordVtyDevTextField.textProperty().isEmpty());
                        }
                    });

                } else {
                    passwordVtyDevTextField.clear();
                    usernameSSHTelnetTextField.clear();
                    isUserCheckBox.setSelected(false);
                    usernameSSHTelnetHbox.setDisable(false);
                    radioButtonsBinding(false, usernameSSHTelnetTextField.textProperty().isEmpty()
                            .or(passwordSSHTextField.textProperty().isEmpty()));
                }
            });
    }

    private void radioButtonsBinding(boolean value, BooleanBinding booleanBinding) {
        passwordVTYHbox.setDisable(!value);
        isUserCheckBox.setDisable(!value);
        usernameSSHTelnetHbox.setDisable(value);
        passwordSSHHbox.setDisable(value);

        addDeviceButton.disableProperty().bind(booleanBinding);
    }

    @FXML
    void selectedTftpServer() {
        deviceModel.addSelectedTftpServer(tftpServerComboBox.getSelectionModel().getSelectedItem());
    }

    @FXML
    void addDevice() {
        String ip = ipTextField.getText();
        String name = Calendar.getInstance().getTimeInMillis() + "-" + nameDeviceTextField.getText();
        String passwordVty = PasswordCrypt.encrypt(passwordVtyDevTextField.getText());
        String password = PasswordCrypt.encrypt(passwordDevTextFiled.getText());
        String connectionType = selectedConnection;
        String passwordSSH = null;
        String loginSSH = null;

        if (connectionType.equals("Telnet") && isUserCheckBox.isSelected()) {
            loginSSH = PasswordCrypt.encrypt(usernameSSHTelnetTextField.getText());
        } else if (connectionType.equals("SSH")) {
            loginSSH = PasswordCrypt.encrypt(usernameSSHTelnetTextField.getText());
            passwordSSH = PasswordCrypt.encrypt(passwordSSHTextField.getText());
        }

        DeviceFx deviceFx = deviceModel.getDeviceByIp(oldIP);
        if (deviceModel.getDeviceEdit() != null || deviceFx != null) {
            deviceFx.setName(nameDeviceTextField.getText());
            deviceFx.setPassword(password);
            deviceFx.setIp(ip);
            deviceFx.setPasswordVty(passwordVty);
            deviceFx.setPasswordSSH(passwordSSH);
            deviceFx.setLoginSSH(loginSSH);
            deviceFx.setConnectionType(connectionType);
            deviceModel.selectedServerTftpProperty().setValue(tftpServerComboBox.getSelectionModel().getSelectedItem());
        } else
            deviceFx = new DeviceFx(ip, name, passwordVty, password, passwordSSH, loginSSH, connectionType);


        log.info("DEVICE ADDED: " + deviceFx.toString());
        // save the device (DATABASE)
        if (isValidIp(ip)) {
            deviceModel.saveDevice(deviceFx);
            backupController.setTableView(deviceModel.getAllUserDevices());

            clear();
        } else {
            Platform.runLater(() -> AlertWindow.error("Nieprawidłowy format adresu IP !", "Błąd"));
        }
    }

    @FXML
    void exit(ActionEvent actionEvent) {
        deviceModel.setDeviceEdit(null);
        oldIP = "";
        WindowUtils.closeView(actionEvent);
    }

    @FXML
    void exitBut(ActionEvent actionEvent) {
        deviceModel.setDeviceEdit(null);
        oldIP = "";
        WindowUtils.closeView(actionEvent);
    }

    @FXML
    public void minimize(ActionEvent event) {
        WindowUtils.minimizeView(event);
    }


    private boolean isValidIp(String address) {
        Pattern pattern = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
        Matcher matcher = pattern.matcher(address);
        return matcher.find();
    }

    private void initBindings() {
        nameDeviceTextField.disableProperty().bind(ipTextField.textProperty().isEmpty());
        tftpServerComboBox.disableProperty().bind(nameDeviceTextField.textProperty().isEmpty());
        passwordDevTextFiled.disableProperty().bind(tftpServerComboBox.getSelectionModel().selectedIndexProperty().lessThan(0));
        radioButtonVbox.disableProperty().bind(passwordDevTextFiled.textProperty().isEmpty());
        addDeviceButton.disableProperty().bind(passwordVtyDevTextField.textProperty().isEmpty()
                .and(passwordSSHTextField.textProperty().isEmpty()));
    }

    private void clear() {
        ipTextField.clear();
        nameDeviceTextField.clear();
        passwordVtyDevTextField.clear();
        passwordDevTextFiled.clear();
        passwordSSHTextField.clear();
        tftpServerComboBox.getSelectionModel().clearSelection();
        usernameSSHTelnetTextField.clear();
        //radioButton.getSelectedToggle().setSelected(false);
        isUserCheckBox.setSelected(false);
    }
}