package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.text.Text;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.modelfx.ListOfDevicesFx;
import pl.project.modelfx.ListOfDevicesModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Konrad
 * date 03/01/2019
 * project_name Backup_Network_Devices
 */
@Component
public class ReadFromListController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    @FXML
    private JFXListView<ListOfDevicesFx> listView;

    @FXML
    private Text isEmptyText;

    private ListOfDevicesModel listOfDevicesModel;

    @Autowired
    private BackupController backupController;


    @Autowired
    public ReadFromListController(ListOfDevicesModel listOfDevicesModel) {
        this.listOfDevicesModel = listOfDevicesModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("ReadFromListController initialize");

        isEmptyText.setVisible(false);

        // add  all User lists to ListView
        loadListView();


    }

    @FXML
    void deleteList() {
        String name = String.valueOf(listView.getSelectionModel().getSelectedItem());
        Optional<ButtonType> buttonType = AlertWindow.confirmation("Usunąć listę " + name + "?", "Uwaga!","");

        if (buttonType.isPresent() && buttonType.get().getButtonData().getTypeCode().equals("Y")) {
            listOfDevicesModel.deleteListOfDevices(name);
            loadListView();

            backupController.setTableView(backupController.deviceModel.getAllUserDevices());
            AlertWindow.information("Usunięto!", "Potwierdzenie");
        }
    }

    @FXML
    public void selectedItem() {
        log.info("selectedItem LIST");

        // always updated if the selected list is changed
        backupController.listenCheckbox();

        listOfDevicesModel.setDevicesInList(String.valueOf(listView.getSelectionModel().getSelectedItem()));
        backupController.setTableView(listOfDevicesModel.getChosenDeviceInList());

    }

    private void loadListView() {
        listView.getItems().clear();
        List list = listOfDevicesModel.getAllUserListOfDevices()
                .stream()
                .map(ListOfDevicesFx::getName)
                .collect(Collectors.toList());
        listView.getItems().addAll(list);

        isListEmpty();
    }

    private void isListEmpty() {
        if (listView.getItems().isEmpty()) {
            isEmptyText.setVisible(true);
            listView.setVisible(false);
        }
    }

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }


    @FXML
    void ok(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    public void minimize(ActionEvent event) {
        WindowUtils.minimizeView(event);
    }
}
