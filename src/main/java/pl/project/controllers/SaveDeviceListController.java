package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.Device;
import pl.project.modelfx.DeviceFx;
import pl.project.modelfx.DeviceModel;
import pl.project.modelfx.ListOfDevicesFx;
import pl.project.modelfx.ListOfDevicesModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.AuthenticationUtils;
import pl.project.utils.DeviceConverter;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 24/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class SaveDeviceListController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    @FXML
    private JFXTextField nameListTextField;

    @FXML
    private JFXButton addListButton;

    @FXML
    private Label checkedValueLabel;

    private DeviceModel deviceModel;

    private ListOfDevicesModel listOfDevicesModel;

    private AuthenticationUtils authenticationUtils;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        checkedValueLabel.setText(String.valueOf(deviceModel.getSelectedList().size()));
        addListButton.disableProperty().bind(nameListTextField.textProperty().isEmpty());
    }

    @Autowired
    SaveDeviceListController(DeviceModel deviceModel, ListOfDevicesModel listOfDevicesModel, AuthenticationUtils authenticationUtils) {
        this.deviceModel = deviceModel;
        this.listOfDevicesModel = listOfDevicesModel;
        this.authenticationUtils = authenticationUtils;
    }

    @FXML
    void addDevice() {
        ObservableSet<DeviceFx> selectedDevices = deviceModel.getSelectedList();
        String name = nameListTextField.getText();

        ListOfDevicesFx listOfDevicesFx = new ListOfDevicesFx(name, authenticationUtils.getAuthenticatedUser());

        for (DeviceFx item : selectedDevices) {
            Device device = DeviceConverter.convertToDevice(item);
            listOfDevicesFx.addDeviceToList(device);
        }

        listOfDevicesModel.saveListOfDevices(listOfDevicesFx);
        nameListTextField.clear();
        AlertWindow.information("Lista została dodana","Dodano listę");

    }

    @FXML
    public void closeView(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    public void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    public void minimize(ActionEvent event) {
        WindowUtils.minimizeView(event);
    }
}
