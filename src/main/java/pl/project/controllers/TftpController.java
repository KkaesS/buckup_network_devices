package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.modelfx.TftpFx;
import pl.project.modelfx.TftpModel;
import pl.project.utils.AlertWindow;
import pl.project.utils.AuthenticationUtils;
import pl.project.utils.WindowUtils;

import java.net.URL;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Konrad
 * date 11/01/2019
 * project_name Backup_Network_Devices
 */
@Component
public class TftpController implements Initializable {


    @FXML
    private JFXTextField ipServerTextField;

    @FXML
    private Text invalidIpText;

    @FXML
    private JFXButton confirmTftpButton;

    @FXML
    private JFXTextField serverNameTextField;

    @FXML
    private TableView<TftpFx> tftpServerTableView;

    @FXML
    private TableColumn<TftpFx, String> serverNameColumn;

    @FXML
    private TableColumn<TftpFx, String> ipServerColumn;

    private TftpModel tftpModel;

    @Autowired
    private BackupController backupController;

    @Autowired
    public TftpController(TftpModel tftpModel,BackupController backupController) {
        this.tftpModel = tftpModel;
        this.backupController = backupController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        invalidIpText.setVisible(false);
        confirmTftpButton.setDisable(true);

        tftpServerTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tftpServerTableView.setPlaceholder(new Label("Brak dostępnych serwerów TFTP."));
        setTableView(tftpModel.getUserTftpServerList());

        // add listener
        tftpServerTableView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) ->
                tftpModel.setServerEdit(newValue));

        // setting an editable table
        serverNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        ipServerColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        ipServerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!isValidIp(newValue)) {
                invalidIpText.setVisible(true);
                confirmTftpButton.setDisable(true);
            } else {
                invalidIpText.setVisible(false);
                confirmTftpButton.setDisable(false);
            }
        });
    }

    @FXML
    void editServerIp(TableColumn.CellEditEvent<TftpFx, String> editEvent) {
        // edit (in db) if the user is authenticated
        if (isValidIp(editEvent.getNewValue())) {
            tftpModel.getServerEdit().setIp(editEvent.getNewValue());
            tftpModel.saveEditServerTftp();
            backupController.setTableView(backupController.deviceModel.getAllUserDevices());

        } else {
            AlertWindow.error("Nieprawidłowy format adresu IP !", "Błąd");
            setTableView(tftpModel.getUserTftpServerList());
        }
    }

    @FXML
    void editServerName(TableColumn.CellEditEvent<TftpFx, String> editEvent) {
        tftpModel.getServerEdit().setName(editEvent.getNewValue());
        tftpModel.saveEditServerTftp();
        backupController.setTableView(backupController.deviceModel.getAllUserDevices());
    }


    @FXML
    void deleteTftp() {
        if (tftpModel.isTftpInUse(tftpModel.getServerEdit())) {
            AlertWindow.error("Nie można usunąć serwera, ponieważ jest przypisany do innego urządzenia", "Błąd");
        } else {
            Optional<ButtonType> alert = AlertWindow.warring("Czy na pewno chcesz usunąć urządzenie?", "Uwaga");
            if (alert.isPresent() && alert.get().getButtonData().getTypeCode().equals("O")) {
                tftpModel.deleteServerTftp();
                setTableView(tftpModel.getUserTftpServerList());
            }
        }
    }


    @FXML
    void confirmTftp() {
        TftpFx tftpFx = new TftpFx(ipServerTextField.getText(), serverNameTextField.getText());
        tftpModel.saveTftpServer(tftpFx);
        setTableView(tftpModel.getUserTftpServerList());

        ipServerTextField.clear();
        serverNameTextField.clear();
        invalidIpText.setVisible(false);
    }

    @FXML
    void back(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    void exit(ActionEvent event) {
        WindowUtils.closeView(event);
    }

    @FXML
    void minimize(ActionEvent event) {
        WindowUtils.minimizeView(event);
    }

    private void setTableView(ObservableList<TftpFx> list) {
        tftpServerTableView.setItems(list);
        serverNameColumn.setCellValueFactory(param -> param.getValue().nameProperty());
        ipServerColumn.setCellValueFactory(param -> param.getValue().ipProperty());
        tftpServerTableView.setVisible(true);
    }

    private boolean isValidIp(String address) {
        Pattern pattern = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
        Matcher matcher = pattern.matcher(address);
        return matcher.find();
    }

}
