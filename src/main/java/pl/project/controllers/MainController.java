package pl.project.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.modelfx.AuthoritiesFx;
import pl.project.modelfx.AuthoritiesModel;
import pl.project.modelfx.UserFx;
import pl.project.modelfx.UserModel;
import pl.project.utils.*;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 08/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class MainController implements Initializable {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());
    private static final String BACKUP_VIEW = "/view/backup/backupView.fxml";
    private static final String LOGIN_VIEW = "/view/main/loginView.fxml";
    private static final String SETTINGS_VIEW = "/view/tftpView.fxml";
    private static final String ABOUT_VIEW = "/view/aboutView.fxml";

    private FxmlUtils fxmlUtils;
    private UserModel userModel;
    private AuthoritiesModel authoritiesModel;

    @FXML
    private JFXSpinner spinner;

    @FXML
    private JFXButton loginButton;

    @Autowired
    MainController(FxmlUtils fxmlUtils, UserModel userModel, AuthoritiesModel authoritiesModel) {
        this.fxmlUtils = fxmlUtils;
        this.userModel = userModel;
        this.authoritiesModel = authoritiesModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("main controller in action!");
        saveAnonymous();
        spinner.setVisible(false);

        if (!AuthenticationUtils.isAuthenticated())
            loginButton.setText("Zaloguj");
        else
            loginButton.setText("Wyloguj");
    }

    private void saveAnonymous() {
        UserFx userFx = new UserFx("anonymous", "", 1);
        AuthoritiesFx authoritiesFx = new AuthoritiesFx(UserConverter.convertToUser(userFx), "ANONYMOUS");
        userModel.saveUser(userFx);
        authoritiesModel.saveAuthority(authoritiesFx);
    }

    @FXML
    void about() {
        fxmlUtils.openView(ABOUT_VIEW);
    }

    @FXML
    void loginAction() {
        if (AuthenticationUtils.isAuthenticated()) {
            Optional<ButtonType> buttonType = AlertWindow.warring("Czy na pewno chcesz się wylogować?", "Uwaga!");
            if (buttonType.isPresent() && buttonType.get().getButtonData().getTypeCode().equals("O")) {
                AuthenticationUtils.logout();
                loginButton.setText("Zaloguj");
                AlertWindow.information("Zostałeś poprawnie wylogowany.", "Wylogowano");
            }
        } else
            fxmlUtils.openView(LOGIN_VIEW);

    }

    @FXML
    public void backupView() {
        Task task = openView(BACKUP_VIEW);
        new Thread(task).start();
    }

    @FXML
    void settingsView() {
        Task task = openView(SETTINGS_VIEW);
        new Thread(task).start();
    }

    private Task openView(String path) {
        return new Task() {
            @Override
            protected Object call() {
                spinner.setVisible(true);
                fxmlUtils.openView(path);
                spinner.setVisible(false);
                return null;
            }
        };
    }

    @FXML
    private void exit() {
        spinner.setVisible(true);
        WindowUtils.exitApp();
        spinner.setVisible(false);
    }

    public JFXButton getLoginButton() {
        return loginButton;
    }
}