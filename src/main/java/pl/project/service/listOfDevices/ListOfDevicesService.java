package pl.project.service.listOfDevices;

import pl.project.entity.ListOfDevices;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 29/12/2018
 * project_name Backup_Network_Devices
 */
public interface ListOfDevicesService {

    void saveList(ListOfDevices newListOfDevices);

    ListOfDevices getList(int id);

    ListOfDevices getList(String name);

  //  ListOfDevices getListByUser(User user);

    List<ListOfDevices> getAllUserList(User user);

    void deleteList(String name);

}
