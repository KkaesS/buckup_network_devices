package pl.project.service.listOfDevices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.project.dao.listOfDevices.ListOfDevicesDAO;
import pl.project.entity.ListOfDevices;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 29/12/2018
 * project_name Backup_Network_Devices
 */
@Service
public class ListOfDevicesServiceImpl implements ListOfDevicesService {

    private ListOfDevicesDAO listOfDevicesDAO;

    @Autowired
    ListOfDevicesServiceImpl(ListOfDevicesDAO listOfDevicesDAO) {
        this.listOfDevicesDAO = listOfDevicesDAO;
    }

    @Override
    @Transactional
    public void saveList(ListOfDevices newListOfDevices) {
        listOfDevicesDAO.saveList(newListOfDevices);
    }

    @Override
    @Transactional
    public ListOfDevices getList(int id) {
        return listOfDevicesDAO.getList(id);
    }

    @Override
    @Transactional
    public ListOfDevices getList(String name) {
        return listOfDevicesDAO.getList(name);
    }

    @Override
    @Transactional
    public List<ListOfDevices> getAllUserList(User user) {
        return listOfDevicesDAO.getAllUserList(user);
    }

    @Override
    @Transactional
    public void deleteList(String name) {
        listOfDevicesDAO.deleteList(name);
    }

}
