package pl.project.service.tftp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.project.dao.tftp.TftpDAO;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */
@Service
public class TftpServiceImpl implements TftpService {

    private TftpDAO tftpDAO;

    @Autowired
    public TftpServiceImpl(TftpDAO tftpDAO) {
        this.tftpDAO = tftpDAO;
    }

    @Override
    @Transactional
    public void saveTftp(Tftp tftp) {
        tftpDAO.saveTftp(tftp);
    }

    @Override
    @Transactional
    public void deleteTftp(Tftp tftp) {
        tftpDAO.deleteTftp(tftp);
    }

    @Override
    @Transactional
    public List<Tftp> getTftpListByUser(User user) {
        return tftpDAO.getTftpListByUser(user);
    }

    @Override
    @Transactional
    public void deleteAllUserTftp(User user) {
        tftpDAO.deleteAllUserTftp(user);
    }

    @Override
    @Transactional
    public boolean isTftpInUse(Tftp tftp) {
        return tftpDAO.isTftpInUse(tftp);
    }
}
