package pl.project.service.authorities;

import pl.project.entity.Authorities;

/**
 * @author Konrad
 * date 20/12/2018
 * project_name Backup_Network_Devices
 */
public interface AuthoritiesService {
    void saveAuthority(Authorities newAuthorities);
}
