package pl.project.service.authorities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.project.dao.authorities.AuthoritiesDAO;
import pl.project.entity.Authorities;

/**
 * @author Konrad
 * date 20/12/2018
 * project_name Backup_Network_Devices
 */

@Service
public class AuthoritiesServiceImp implements AuthoritiesService {

    private AuthoritiesDAO authoritiesDAO;

    @Autowired
    AuthoritiesServiceImp(AuthoritiesDAO authoritiesDAO) {
        this.authoritiesDAO = authoritiesDAO;
    }

    @Override
    @Transactional
    public void saveAuthority(Authorities newAuthorities) {
        authoritiesDAO.saveAuthority(newAuthorities);
    }
}
