package pl.project.service.device;

import pl.project.entity.Device;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */
public interface DeviceService {

    List<Device> getDevices(User user);

    void saveDevice(Device newDevice);

    Device getDevice(String ip);

    void deleteDevice(Device device);

    void deleteAllUserDevices(User user);

}
