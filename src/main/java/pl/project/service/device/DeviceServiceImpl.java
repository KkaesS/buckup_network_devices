package pl.project.service.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.project.dao.device.DeviceDAO;
import pl.project.entity.Device;
import pl.project.entity.Tftp;
import pl.project.entity.User;

import java.beans.Transient;
import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */

@Service
public class DeviceServiceImpl implements DeviceService {

    private DeviceDAO deviceDAO;

    @Autowired
    DeviceServiceImpl(DeviceDAO deviceDAO) {
        this.deviceDAO = deviceDAO;
    }

    @Override
    @Transactional
    public List<Device> getDevices(User user) {
        return deviceDAO.getDevices(user);
    }

    @Override
    @Transactional
    public void saveDevice(Device newDevice) {
        deviceDAO.saveDevice(newDevice);
    }

    @Override
    @Transactional
    public Device getDevice(String ip) {
        return deviceDAO.getDevice(ip);
    }

    @Override
    @Transactional
    public void deleteDevice(Device device) {
        deviceDAO.deleteDevice(device);
    }

    @Override
    @Transactional
    public void deleteAllUserDevices(User user) {
        deviceDAO.deleteAllUserDevices(user);
    }
}
