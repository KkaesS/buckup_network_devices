package pl.project.service.user;

import pl.project.entity.Device;
import pl.project.entity.User;

import java.util.List;

/**
 * @author Konrad
 * date 12/12/2018
 * project_name Backup_Network_Devices
 */
public interface UserService {

    List<User> getUsers();

    void saveUser(User newUser);

    User findByEmail(String email);

    User getUser(String username);

    void deleteUser(int id);
}
