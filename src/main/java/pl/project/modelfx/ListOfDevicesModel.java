package pl.project.modelfx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.Device;
import pl.project.entity.ListOfDevices;
import pl.project.service.listOfDevices.ListOfDevicesService;
import pl.project.utils.AuthenticationUtils;
import pl.project.utils.DeviceConverter;
import pl.project.utils.ListOfDevicesConverter;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 03/01/2019
 * project_name Backup_Network_Devices
 */
@Component
public class ListOfDevicesModel {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private ListOfDevicesService listOfDevicesService;

    private AuthenticationUtils authenticationUtils;

    private ObservableList<ListOfDevicesFx> usersListOfDevices = FXCollections.observableArrayList();

    private ObservableList<DeviceFx> chosenDeviceInList = FXCollections.observableArrayList();


    @Autowired
    public ListOfDevicesModel(ListOfDevicesService listOfDevicesService, AuthenticationUtils auth) {
        this.listOfDevicesService = listOfDevicesService;
        this.authenticationUtils = auth;
    }

    public void deleteListOfDevices(String name) {
        listOfDevicesService.deleteList(name);
    }

    // save the list of devices
    public void saveListOfDevices(ListOfDevicesFx listOfDevicesFx) {
        listOfDevicesService.saveList(ListOfDevicesConverter.convertToListOfDevices(listOfDevicesFx));
    }

    public ObservableList<ListOfDevicesFx> getAllUserListOfDevices() {
        List<ListOfDevices> list = listOfDevicesService.getAllUserList(authenticationUtils.getAuthenticatedUser());
        usersListOfDevices.clear();

        for (ListOfDevices item : list) {
            ListOfDevicesFx listOfDevicesFx = ListOfDevicesConverter.convertToListOfDevicesFx(item);
            usersListOfDevices.add(listOfDevicesFx);
        }

        return usersListOfDevices;
    }

    public void setDevicesInList(String name) {
        List<Device> list = listOfDevicesService.getList(name).getDevices();
        chosenDeviceInList.clear();

        for (Device device : list) {
            DeviceFx deviceFx = DeviceConverter.convertToDeviceFx(device);
            chosenDeviceInList.add(deviceFx);
        }
    }

    public ListOfDevicesService getListOfDevicesService() {
        return listOfDevicesService;
    }

    public void setListOfDevicesService(ListOfDevicesService listOfDevicesService) {
        this.listOfDevicesService = listOfDevicesService;
    }

    public ObservableList<ListOfDevicesFx> getUsersListOfDevices() {
        return usersListOfDevices;
    }

    public void setUsersListOfDevices(ObservableList<ListOfDevicesFx> usersListOfDevices) {
        this.usersListOfDevices = usersListOfDevices;
    }

    public AuthenticationUtils getAuthenticationUtils() {
        return authenticationUtils;
    }

    public void setAuthenticationUtils(AuthenticationUtils authenticationUtils) {
        this.authenticationUtils = authenticationUtils;
    }

    public ObservableList<DeviceFx> getChosenDeviceInList() {
        return chosenDeviceInList;
    }

    public void setChosenDeviceInList(ObservableList<DeviceFx> chosenDeviceInList) {
        this.chosenDeviceInList = chosenDeviceInList;
    }

}
