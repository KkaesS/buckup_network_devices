package pl.project.modelfx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.Authorities;
import pl.project.service.authorities.AuthoritiesService;
import pl.project.utils.AuthoritiesConverter;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
@Component
public class AuthoritiesModel {

    private AuthoritiesService authoritiesService;

    @Autowired
    public AuthoritiesModel(AuthoritiesService authoritiesService) {
        this.authoritiesService = authoritiesService;
    }

    public void saveAuthority(AuthoritiesFx authoritiesFx) {
        Authorities authorities = AuthoritiesConverter.convertToAuthorities(authoritiesFx);
        authoritiesService.saveAuthority(authorities);
    }
}
