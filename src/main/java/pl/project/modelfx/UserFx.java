package pl.project.modelfx;

import javafx.beans.property.*;
import org.springframework.stereotype.Component;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
public class UserFx {

    private StringProperty username = new SimpleStringProperty(this, "username");

    private StringProperty password = new SimpleStringProperty();

    private IntegerProperty enabled = new SimpleIntegerProperty(1);

    public UserFx() {
        this("Not set", "Not set",  1);
    }

    public UserFx(String username, String password , int enabled) {
        setUsername(username);
        setEnabled(enabled);
        setPassword(password);
    }

    public UserFx(StringProperty username, StringProperty password, IntegerProperty enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public int getEnabled() {
        return enabled.get();
    }

    public IntegerProperty enabledProperty() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled.set(enabled);
    }
}
