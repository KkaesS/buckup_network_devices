package pl.project.modelfx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.springframework.stereotype.Component;
import pl.project.entity.User;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
public class AuthoritiesFx {

    private ObjectProperty<User> user = new SimpleObjectProperty<>();

    private StringProperty authority = new SimpleStringProperty();

    public AuthoritiesFx() {
        this(new User(), "anonymous");
    }

    public AuthoritiesFx(User user, String authority) {
        setUser(user);
        setAuthority(authority);
    }

    public AuthoritiesFx(ObjectProperty<User> user, StringProperty authority) {
        this.user = user;
        this.authority = authority;
    }

    public User getUser() {
        return user.get();
    }

    public ObjectProperty<User> userProperty() {
        return user;
    }

    public void setUser(User user) {
        this.user.set(user);
    }

    public String getAuthority() {
        return authority.get();
    }

    public StringProperty authorityProperty() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority.set(authority);
    }
}
