package pl.project.modelfx;

import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.User;
import pl.project.service.authorities.AuthoritiesService;
import pl.project.service.device.DeviceService;
import pl.project.service.tftp.TftpService;
import pl.project.service.user.UserService;
import pl.project.utils.UserConverter;

import java.util.logging.Logger;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
@Component
public class UserModel {
    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private UserService userService;
    private TftpService tftpService;
    private DeviceService deviceService;

    @Autowired
    public UserModel(UserService userService, TftpService tftpService, DeviceService deviceService) {
        this.userService = userService;
        this.tftpService = tftpService;
        this.deviceService = deviceService;
    }

    public void saveUser(UserFx userFx) {
        log.info("user saved");
        User user = UserConverter.convertToUser(userFx);
        userService.saveUser(user);
    }

    public void deleteAnonymousFields() {
        log.info("deleting anonymous staff");
        deviceService.deleteAllUserDevices(userService.getUser("anonymous"));
        tftpService.deleteAllUserTftp(userService.getUser("anonymous"));
    }
}
