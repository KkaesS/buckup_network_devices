package pl.project.modelfx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javassist.bytecode.stackmap.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.Device;
import pl.project.entity.User;
import pl.project.service.device.DeviceService;
import pl.project.service.listOfDevices.ListOfDevicesService;
import pl.project.service.user.UserService;
import pl.project.utils.*;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * @author Konrad
 * date 14/12/2018
 * project_name Backup_Network_Devices
 */
@Component
public class DeviceModel {

    private final Logger log = Logger.getLogger(TypeData.ClassName.class.getName());

    private ObjectProperty<DeviceFx> deviceEdit = new SimpleObjectProperty<>();

    private ObjectProperty<TftpFx> selectedServerTftp = new SimpleObjectProperty<>(new TftpFx());

    private ObservableSet<DeviceFx> selectedList = FXCollections.observableSet(new HashSet<>());

    private ObservableList<DeviceFx> deviceObservableList = FXCollections.observableArrayList();

    private DeviceService deviceService;

    private UserService userService;

    private ListOfDevicesService listOfDevicesService;

    private AuthenticationUtils authenticationUtils;

    @Autowired
    DeviceModel(DeviceService deviceService, UserService userService, ListOfDevicesService list, AuthenticationUtils auth) {
        this.deviceService = deviceService;
        this.userService = userService;
        this.listOfDevicesService = list;
        this.authenticationUtils = auth;
    }

    public void init() {
        // only load the listView if the user is authenticated
        System.out.println("authenticated? " + AuthenticationUtils.isAuthenticated());
        getAllUserDevices();
    }

    public void addSelectedTftpServer(TftpFx tftpFx) {
        selectedServerTftp.setValue(tftpFx);
    }

    public void deleteDevice(DeviceFx deviceFx) {
        deviceService.deleteDevice(DeviceConverter.convertToDevice(deviceFx));
        getAllUserDevices();
    }

    @NotNull
    public ObservableList<DeviceFx> getAllUserDevices() {
        // get a list of devices by username
        List<Device> deviceList;
        deviceObservableList.clear();
        selectedList.clear();

        if (AuthenticationUtils.isAuthenticated())
            deviceList = deviceService.getDevices(authenticationUtils.getAuthenticatedUser());
        else
            deviceList = deviceService.getDevices(userService.getUser("anonymous"));

        // add to deviceObservableList
        deviceList.forEach(item -> {
            DeviceFx deviceFx = DeviceConverter.convertToDeviceFx(item);
            deviceObservableList.add(deviceFx);
        });

        return deviceObservableList;
    }


    public void addSelectedDevice(@NotNull DeviceFx deviceFx) {
        selectedList.add(deviceFx);
        log.info("selected add Items " + selectedList.size() + "\n" + selectedList);
    }

    public void removeSelectedDevice(@NotNull DeviceFx deviceFx) {
        selectedList.remove(deviceFx);
        log.info("selected remove Items " + selectedList.size() + "\n" + selectedList);
    }

    public DeviceFx getDeviceByIp(String ip) {
        Optional<Device> isExist = Optional.ofNullable(deviceService.getDevice(ip));
        return isExist.map(DeviceConverter::convertToDeviceFx).orElse(null);
    }

    // save user devices in DATABASE
    public void saveDevice(DeviceFx deviceFx) {
        createDevice(deviceFx);
    }

    // update device
    public void saveEditDevice() {
        deviceService.saveDevice(DeviceConverter.convertToDevice(getDeviceEdit()));
    }

    // save device
    private void createDevice(DeviceFx devicefx) {
        User currentUser;

        if (AuthenticationUtils.isAuthenticated())
            currentUser = authenticationUtils.getAuthenticatedUser();
        else
            currentUser = userService.getUser("anonymous");

        // assign a user and the tftp to device
        devicefx.setUser(currentUser);
        devicefx.setTftp(TftpConverter.convertToTftp(getSelectedServerTftp()));

        deviceService.saveDevice(DeviceConverter.convertToDevice(devicefx));
    }

    public ObservableList<DeviceFx> getDeviceObservableList() {
        return deviceObservableList;
    }

    public void setDeviceObservableList(ObservableList<DeviceFx> deviceObservableList) {
        this.deviceObservableList = deviceObservableList;
    }

    public DeviceFx getDeviceEdit() {
        return deviceEdit.get();
    }

    public ObjectProperty<DeviceFx> deviceEditProperty() {
        return deviceEdit;
    }

    public void setDeviceEdit(DeviceFx deviceEdit) {
        this.deviceEdit.set(deviceEdit);
    }

    public DeviceService getDeviceService() {
        return deviceService;
    }

    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ListOfDevicesService getListOfDevicesService() {
        return listOfDevicesService;
    }

    public void setListOfDevicesService(ListOfDevicesService listOfDevicesService) {
        this.listOfDevicesService = listOfDevicesService;
    }

    public Logger getLog() {
        return log;
    }

    public ObservableSet<DeviceFx> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(ObservableSet<DeviceFx> selectedList) {
        this.selectedList = selectedList;
    }

    public AuthenticationUtils getAuthenticationUtils() {
        return authenticationUtils;
    }

    public void setAuthenticationUtils(AuthenticationUtils authenticationUtils) {
        this.authenticationUtils = authenticationUtils;
    }

    public TftpFx getSelectedServerTftp() {
        return selectedServerTftp.get();
    }

    public ObjectProperty<TftpFx> selectedServerTftpProperty() {
        return selectedServerTftp;
    }

}
