package pl.project.modelfx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.project.entity.Tftp;
import pl.project.entity.User;
import pl.project.service.device.DeviceService;
import pl.project.service.tftp.TftpService;
import pl.project.service.user.UserService;
import pl.project.utils.AuthenticationUtils;
import pl.project.utils.DeviceConverter;
import pl.project.utils.TftpConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */

@Component
public class TftpModel {

    private ObservableList<TftpFx> tftpList = FXCollections.observableArrayList();

    private TftpService tftpService;

    private DeviceService deviceService;

    private ObjectProperty<TftpFx> serverEdit = new SimpleObjectProperty<>(new TftpFx());

    private AuthenticationUtils authenticationUtils;

    private UserService userService;

    @Autowired
    public TftpModel(TftpService tftpService, AuthenticationUtils authenticationUtils, UserService userService,
                     DeviceService deviceService) {
        this.tftpService = tftpService;
        this.authenticationUtils = authenticationUtils;
        this.userService = userService;
        this.deviceService = deviceService;
    }

    public void deleteServerTftp() {
        tftpService.deleteTftp(TftpConverter.convertToTftp(getServerEdit()));
    }

    // update tftp
    public void saveEditServerTftp() {
        tftpService.saveTftp(TftpConverter.convertToTftp(getServerEdit()));
    }

    // if is authenticated
    public void saveTftpServer(TftpFx tftpFx) {
        User currentUser;

        if (AuthenticationUtils.isAuthenticated())
            currentUser = authenticationUtils.getAuthenticatedUser();
        else
            currentUser = userService.getUser("anonymous");

        // assign a user to the tftp
        tftpFx.setUser(currentUser);
        tftpService.saveTftp(TftpConverter.convertToTftp(tftpFx));
    }


    public ObservableList<TftpFx> getUserTftpServerList() {
        List<Tftp> tftp = null;
        if (AuthenticationUtils.isAuthenticated())
            tftp = tftpService.getTftpListByUser(authenticationUtils.getAuthenticatedUser());
        else
            tftp = tftpService.getTftpListByUser(userService.getUser("anonymous"));

        tftpList.clear();
        tftp.forEach(item -> {
            TftpFx tftpFx = TftpConverter.convertToTftpFx(item);
            tftpList.add(tftpFx);
        });
        return tftpList;
    }


    public boolean isTftpInUse(TftpFx tftpFx) {
        return tftpService.isTftpInUse(TftpConverter.convertToTftp(tftpFx));
    }

    public ObservableList<TftpFx> getTftpList() {
        return tftpList;
    }

    public TftpFx getServerEdit() {
        return serverEdit.get();
    }

    public ObjectProperty<TftpFx> serverEditProperty() {
        return serverEdit;
    }

    public void setServerEdit(TftpFx serverEdit) {
        this.serverEdit.set(serverEdit);
    }
}
