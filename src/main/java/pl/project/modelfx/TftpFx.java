package pl.project.modelfx;

import javafx.beans.property.*;
import pl.project.entity.User;

import java.io.Serializable;

/**
 * @author Konrad
 * date 18/01/2019
 * project_name Backup_Network_Devices
 */

public class TftpFx implements Serializable {

    private IntegerProperty id = new SimpleIntegerProperty();

    private StringProperty name = new SimpleStringProperty();

    private StringProperty ip = new SimpleStringProperty();

    private ObjectProperty<User> user = new SimpleObjectProperty<>();

    public TftpFx() {
        this("Not set!", "Not set");
    }

    public TftpFx(String ip, String name) {
        setIp(ip);
        setName(name);
    }


    public TftpFx(StringProperty ip, StringProperty name) {
        this.ip = ip;
        this.name = name;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public User getUser() {
        return user.get();
    }

    public ObjectProperty<User> userProperty() {
        return user;
    }

    public void setUser(User user) {
        this.user.set(user);
    }

    public String getIp() {
        return ip.get();
    }

    public StringProperty ipProperty() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip.set(ip);
    }

    @Override
    public String toString() {
        return ip.getValue();
    }
}
