package pl.project.modelfx;

import javafx.beans.property.*;
import pl.project.entity.Tftp;
import pl.project.entity.User;

/**
 * @author Konrad
 * date 13/12/2018
 * project_name Backup_Network_Devices
 */

public class DeviceFx {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty ip = new SimpleStringProperty();
    private BooleanProperty select = new SimpleBooleanProperty(true);
    private StringProperty passwordVty = new SimpleStringProperty();
    private StringProperty password = new SimpleStringProperty();
    private StringProperty passwordSSH = new SimpleStringProperty();
    private StringProperty loginSSH = new SimpleStringProperty();
    private StringProperty connectionType = new SimpleStringProperty();
    private ObjectProperty<User> user = new SimpleObjectProperty<>();
    private ObjectProperty<Tftp> tftp = new SimpleObjectProperty<>();

    public DeviceFx(String ip, String name, String passwordVty, String passwordEnable, String passwordSSH,
                    String loginSSH, String connectionType) {
        setName(name);
        setIp(ip);
        setPasswordVty(passwordVty);
        setPassword(passwordEnable);
        setPasswordSSH(passwordSSH);
        setLoginSSH(loginSSH);
        setConnectionType(connectionType);
    }


    public DeviceFx(StringProperty name, StringProperty ip, StringProperty passwordVty, StringProperty passwordEnable,
                    StringProperty passwordSSH, StringProperty loginSSH,StringProperty connectionType) {
        this.name = name;
        this.ip = ip;
        this.passwordVty = passwordVty;
        this.password = passwordEnable;
        this.passwordSSH = passwordSSH;
        this.loginSSH = loginSSH;
        this.connectionType = connectionType;
    }

    public DeviceFx() {
        this("Not set", "Not set", "Not set", "Not set", "Not set",
                "Not set","Not Set");
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getIp() {
        return ip.get();
    }

    public StringProperty ipProperty() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip.set(ip);
    }

    public User getUser() {
        return user.get();
    }

    public ObjectProperty<User> userProperty() {
        return user;
    }

    public void setUser(User user) {
        this.user.set(user);
    }

    public boolean isSelect() {
        return select.get();
    }

    public BooleanProperty selectProperty() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select.set(select);
    }

    public String getPasswordVty() {
        return passwordVty.get();
    }

    public StringProperty passwordVtyProperty() {
        return passwordVty;
    }

    public void setPasswordVty(String passwordVty) {
        this.passwordVty.set(passwordVty);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public Tftp getTftp() {
        return tftp.get();
    }

    public ObjectProperty<Tftp> tftpProperty() {
        return tftp;
    }

    public void setTftp(Tftp tftp) {
        this.tftp.set(tftp);
    }

    public String getPasswordSSH() {
        return passwordSSH.get();
    }

    public StringProperty passwordSSHProperty() {
        return passwordSSH;
    }

    public void setPasswordSSH(String passwordSSH) {
        this.passwordSSH.set(passwordSSH);
    }

    public String getLoginSSH() {
        return loginSSH.get();
    }

    public StringProperty loginSSHProperty() {
        return loginSSH;
    }

    public void setLoginSSH(String loginSSH) {
        this.loginSSH.set(loginSSH);
    }

    public String getConnectionType() {
        return connectionType.get();
    }

    public StringProperty connectionTypeProperty() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType.set(connectionType);
    }

    @Override
    public String toString() {
        return "DeviceFx{" +
                "id=" + id +
                ", name=" + name +
                ", ip=" + ip +
                ", select=" + select +
                ", passwordVty=" + passwordVty +
                ", password=" + password +
                ", passwordSSH=" + passwordSSH +
                ", loginSSH=" + loginSSH +
                ", connectionType=" + connectionType +
                ", user=" + user +
                ", tftp=" + tftp +
                '}';
    }
}
