package pl.project.modelfx;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.project.entity.Device;
import pl.project.entity.User;

import javax.validation.constraints.NotNull;

/**
 * @author Konrad
 * date 02/01/2019
 * project_name Backup_Network_Devices
 */
public class ListOfDevicesFx {

    private IntegerProperty id = new SimpleIntegerProperty();

    private StringProperty name = new SimpleStringProperty();

    private ObjectProperty<User> user = new SimpleObjectProperty<>();

    private ObservableList<Device> deviceList = FXCollections.observableArrayList();

    public ListOfDevicesFx() {
        this("Not set", new User());
    }

    public ListOfDevicesFx(String name, User user) {
        setName(name);
        setUser(user);
    }

    public ListOfDevicesFx(StringProperty name, ObjectProperty<User> user) {
        this.name = name;
        this.user = user;
    }

    public void addDeviceToList(@NotNull Device device) {
        deviceList.add(device);
    }

    public ObservableList<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(ObservableList<Device> deviceList) {
        this.deviceList = deviceList;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public User getUser() {
        return user.get();
    }

    public ObjectProperty<User> userProperty() {
        return user;
    }

    public void setUser(User user) {
        this.user.set(user);
    }


}
